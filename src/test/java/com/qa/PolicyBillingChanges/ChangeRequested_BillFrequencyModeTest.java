package com.qa.PolicyBillingChanges;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class ChangeRequested_BillFrequencyModeTest extends TestBase{
	public ChangeRequested_BillFrequencyModeTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Change Requested Bill Frequency/Mode 
	  * Purpose : To validate whether user is able to submit Change Requested_Bill Frequency/Mode 
	  * History : Created by Anjali Johny on 10/12/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getChangeRequestedBillFrequencyMode(){
	Object data[][] = TestUtil.getTestData("Bill FrequencyMode");
	return data;
}


@Test(priority=1,dataProvider="getChangeRequestedBillFrequencyMode")
    public void changeReqstedBillFrequencyMode(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,String FirstName,
    		String LastName,String Email,String Phone,String reqEffDate,String activeClaim,String life,String add,String dlf,String dad,String vtl,
		     String vta,String dvt,String dva,String std,String ltd,String wds,String wdl,String ols,String classno,String changeRqst,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Change Requested_Bill Frequency/Mode for "+userRole);
	
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Change Requested_Bill Frequency/Mode for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	policyBillingDetailPageAction=homepageAction.validateLeftNav(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateFileUploadSet4(prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("pdfFilePath"),
			prop.getProperty("xlsxFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("doc5mbsizePath"),prop.getProperty("txtExceedSizePath"),changeRqst);
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	//driver.navigate().refresh();
	homepageAction.ValidateSideLink(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateFileUploadSet5(prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),changeRqst);
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	homepageAction.userLogout();
}


}