package com.qa.PolicyBillingChanges;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class ChangeRequested_MultipleChangeRequestedTest extends TestBase{
	public ChangeRequested_MultipleChangeRequestedTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Change Requested Multiple Changes Requested
	  * Purpose : To validate whether user is able to submit Change Requested_Multiple Changes Requested
	  * History : Created by Anjali Johny on 11/30/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getChangeRequestedMutipleChangesRequested(){
	Object data[][] = TestUtil.getTestData("Multiple Changes Requested");
	return data;
}


@Test(priority=1,dataProvider="getChangeRequestedMutipleChangesRequested")
    public void changeReqstedMultipleChangesRequested(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,String FirstName,
    		String LastName,String Email,String Phone,String reqEffDate,String activeClaim,String life,String add,String dlf,String dad,String vtl,
		     String vta,String dvt,String dva,String std,String ltd,String wds,String wdl,String ols,String classno,String changeRqst,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Change Requested_Multiple Changes Requested for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Change Requested_Multiple Changes Requested for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	policyBillingDetailPageAction=homepageAction.validateLeftNav(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateFileUploadPolicyForm2Set1(prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("pdfFilePath"),
			prop.getProperty("xlsxFilePath"),prop.getProperty("doc5mbsizePath"),prop.getProperty("txtExceedSizePath"),changeRqst);
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	driver.navigate().refresh();
	homepageAction.ValidateSideLink(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateFileUploadPolicyForm2Set2(prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),prop.getProperty("csvFilePath"),changeRqst);
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	driver.navigate().refresh();
	homepageAction.ValidateSideLink(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateFileUploadPolicyForm2Set3(prop.getProperty("txtFilePath"),changeRqst);
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	homepageAction.userLogout();
}


}