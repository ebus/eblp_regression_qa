package com.qa.PolicyBillingChanges;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class ChangeRequested_AddDependentOptionTest extends TestBase{
	public ChangeRequested_AddDependentOptionTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Change Requested Add Dependent Option
	  * Purpose : To validate whether user is able to submit Change Requested_Add Dependent Option
	  * History : Created by Anjali Johny on 10/25/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getChangeRequestedAddDependentOption(){
	Object data[][] = TestUtil.getTestData("Add Dependent Option");
	return data;
}


@Test(priority=1,dataProvider="getChangeRequestedAddDependentOption")
    public void changeReqstedAddDependentOption(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,String FirstName,
    		String LastName,String Email,String Phone,String reqEffDate,String activeClaim,String life,String add,String dlf,String dad,String vtl,
		     String vta,String dvt,String dva,String std,String ltd,String wds,String wdl,String ols,String classno,String changeRqst,String userRole, String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Change Requested_Add Dependent Option for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Change Requested_Add Dependent Option for "+userRole + " is Skipped");
    }
	
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	policyBillingDetailPageAction=homepageAction.validateLeftNav(userRole);
	policyBillingDetailPageAction.validateHomePagewithSelectALLCov(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim
		     , classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateAddDepSet1(changeRqst,prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("pdfFilePath"),
			prop.getProperty("xlsxFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("doc5mbsizePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("xlsxExceedSizePath"));
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	//driver.navigate().refresh();
	homepageAction.ValidateSideLink(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateAddDepSet2(changeRqst,prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),prop.getProperty("xlsxFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("xlsxExceedSizePath"));
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	//driver.navigate().refresh();
	homepageAction.ValidateSideLink(userRole);
	policyBillingDetailPageAction.validateHomePage(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone, reqEffDate, activeClaim, life, add, dlf, dad, vtl,
		      vta, dvt, dva, std, ltd, wds, wdl, ols, classno,changeRqst);
	policyBillingDetailsReviewPageAction=policyBillingDetailPageAction.validateAddDepSet3(changeRqst,prop.getProperty("txtFilePath"),prop.getProperty("xlsxFilePath"),prop.getProperty("docFilePath"),prop.getProperty("xlsxExceedSizePath"));
	confirmationPageAction=policyBillingDetailsReviewPageAction.validateReviewPage();
	policyBillingDetailPageAction=confirmationPageAction.validateConfirmation();
	homepageAction.userLogout();
	
}


}