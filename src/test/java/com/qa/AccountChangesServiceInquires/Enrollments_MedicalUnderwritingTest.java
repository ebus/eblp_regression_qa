package com.qa.AccountChangesServiceInquires;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Enrollments_MedicalUnderwritingTest extends TestBase{
	public Enrollments_MedicalUnderwritingTest() {
		super();	
	}
	
/* ******************************************************************************
	  * Test Name : Enrollments_Medical Underwriting
	  * Purpose : To validate whether user is able to submit Enrollments_Medical Underwriting
	  * History : Created by Anjali Johny on 12/28/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getEnrollmentsMedicalUW(){
	Object data[][] = TestUtil.getTestData("Enrollments Medical UW");
	return data;
}
@Test(priority=1,dataProvider="getEnrollmentsMedicalUW")
    public void EnrollmentsMedicalUW(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Enrollments Medical Underwriting for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Enrollments Medical Underwriting  for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEnrollmentsMedicalUWSet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("docFilePath"),prop.getProperty("txtExceedSizePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEnrollmentsMedicalUWSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("pdfFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEnrollmentsMedicalUWSet3(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("txtFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEnrollmentsMedicalUWSet4(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEnrollmentsMedicalUWSet5(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("csvFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	homepageAction.userLogout();
}


}