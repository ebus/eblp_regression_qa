package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GeneralInquiries_MultipleChangesTest extends TestBase {
	public GeneralInquiries_MultipleChangesTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : General Inquiries - Multiple Changes
	  * Purpose :  To verify the Sub category - Multiple Changes when Request Type is General Inquiries
	  * History : Created by Saivenkat Korivi on 03/24/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getMultipleChanges(){
		Object data[][] = TestUtil.getTestData("Multiple Changes");
		return data;		
}
	@Test(priority=1,dataProvider="getMultipleChanges")
	public void MultipleChanges(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Multiple Changes for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Multiple Changes for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateMultipleChanges(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("txtFilePath"),
			prop.getProperty("xlsxFilePath"),prop.getProperty("pdfFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateMultipleChanges(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("docFilePath"),
			prop.getProperty("pdfFilePath"),prop.getProperty("csvFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
	}
}
