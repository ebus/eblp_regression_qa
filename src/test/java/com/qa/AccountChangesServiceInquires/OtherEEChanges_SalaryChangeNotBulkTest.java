package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OtherEEChanges_SalaryChangeNotBulkTest extends TestBase {
	public OtherEEChanges_SalaryChangeNotBulkTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Other Employee Changes - Salary Change - Not Bulk
	  * Purpose :  To verify the Sub category - Salary Change (Not Bulk) when Request Type is Other Employee Changes
	  * History : Created by Saivenkat Korivi on 03/17/2022 	 
**************************************************************************************/	
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getSalaryChangeNotBulk(){
		Object data[][] = TestUtil.getTestData("Salary Change - Not Bulk");
		return data;		
}
	@Test(priority=1,dataProvider="getSalaryChangeNotBulk")
	public void SalaryChangeNotBulk(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,String eeSSN,String NewSalary, String SalaryType,String NewSalaryEffectiveDate,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Salary Change (Not Bulk) for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Salary Change (Not Bulk) for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSalaryChangeNotBulk(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry, eeFirstName,eeLastName,eeSSN,NewSalary,SalaryType,NewSalaryEffectiveDate);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.userLogout();
	
	}
}
	
