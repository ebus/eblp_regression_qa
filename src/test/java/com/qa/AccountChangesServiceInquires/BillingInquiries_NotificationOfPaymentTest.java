package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class BillingInquiries_NotificationOfPaymentTest extends TestBase{
	public BillingInquiries_NotificationOfPaymentTest() {
		super();
	}
/* *****************************************************************************
	  * Test Name : Billing Inquiries - Notification of Payment
	  * Purpose :  To verify the Sub category - Notification of Payment when Request Type is Billing Inquiries
	  * History : Created by Saivenkat Korivi on 03/23/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getNotificationOfPayment(){
		Object data[][] = TestUtil.getTestData("Notification Of Payment");
		return data;		
}
	@Test(priority=1,dataProvider="getNotificationOfPayment")
	public void NotificationOfPayment(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String PayAmount,String Note,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Billing Inquiries - Notification Of Payment for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Billing Inquiries - Notification Of Payment for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	//File upload Combination 1
		accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNotificationOfPaymentSet1(policyHolderName,
				policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,PayAmount,Note,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("txtFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	//File Upload Combination 2
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNotificationOfPaymentSet2(policyHolderName,
				policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,PayAmount,Note,prop.getProperty("pdfFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	//File Upload Combination 3
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNotificationOfPaymentSet2(policyHolderName,
						policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,PayAmount,Note,prop.getProperty("xlsxFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	//File Upload Combination 4
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNotificationOfPaymentSet2(policyHolderName,
								policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,PayAmount,Note,prop.getProperty("docFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	//File Upload Combination 5
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNotificationOfPaymentSet2(policyHolderName,
										policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,PayAmount,Note,prop.getProperty("csvFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
		
		homepageAction.userLogout();
	}

}
