package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OtherEEChanges_SalaryChangeBulkFullCensusTest extends TestBase {
	public OtherEEChanges_SalaryChangeBulkFullCensusTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Other Employee Changes - Salary Change - Bulk (Full Census)
	  * Purpose :  To verify the Sub category - Salary Change - Bulk (Full Census) when Request Type is Other Employee Changes
	  * History : Created by Saivenkat Korivi on 03/17/2022 	 
**************************************************************************************/	
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getSalaryChangeBulkFullCensus(){
		Object data[][] = TestUtil.getTestData("Salary Change - Bulk_FullCensus");
		return data;		
}
	@Test(priority=1,dataProvider="getSalaryChangeBulkFullCensus")
	public void SalaryChangeBulkFullCensus(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String EffectiveDateOfChange,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Salary Change - Bulk (Full Census) for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Salary Change - Bulk (Full Census) for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	//File Upload Combination 1
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSalaryChangeBulkSet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,EffectiveDateOfChange,prop.getProperty("PNGFilePath"),
			prop.getProperty("csvFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	//File Upload Combination 2
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSalaryChangeBulkSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,EffectiveDateOfChange,
			prop.getProperty("xlsExceedSizePath"),prop.getProperty("xlsxFilePath"),prop.getProperty("csvFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
	}
}
