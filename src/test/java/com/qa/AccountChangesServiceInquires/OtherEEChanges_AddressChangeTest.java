package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OtherEEChanges_AddressChangeTest extends TestBase {
	public OtherEEChanges_AddressChangeTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Other Employee Changes - Address Change
	  * Purpose :  To verify the Sub category - Address Change when Request Type is Other Employee Changes
	  * History : Created by Saivenkat Korivi on 03/18/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getAddressChange(){
		Object data[][] = TestUtil.getTestData("Other EE Changes_Address Change");
		return data;		
}
	@Test(priority=1,dataProvider="getAddressChange")
	public void AddressChange(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,String eeSSN,String eeNewStreetAddress,String eeNewCity,String eeNewState,String eeNewZipCode,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Address Change for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Address Change for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddressChange(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,eeLastName,eeSSN,eeNewStreetAddress,eeNewCity,eeNewState,eeNewZipCode);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.userLogout();
	}
	
}
