package com.qa.AccountChangesServiceInquires;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Enrollments_Reinstatement_EmployerPaidTest extends TestBase{
	public Enrollments_Reinstatement_EmployerPaidTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Enrollments_Reinstatement- Employer Paid
	  * Purpose : To validate whether user is able to submit Enrollments_Reinstatement Employer Paid
	  * History : Created by Anjali Johny on 12/28/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getEnrollmentsReinstatementEmployerPaid(){
	Object data[][] = TestUtil.getTestData("Enrollment ReInsta EmployerPaid");
	return data;
}
@Test(priority=1,dataProvider="getEnrollmentsReinstatementEmployerPaid")
    public void EnrollmentsReinstatementEmployerPaid(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String rehireDate,String eeFirstName,String eeLastName,
    		String eeDOB,String eeSSN,String eeSalary,String salType,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Enrollments_Reinstatement Employer Paid for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Enrollments_Reinstatement Employer Paid for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateReinstatementEmployeePaid(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,rehireDate,eeFirstName,eeLastName,
			eeDOB,eeSSN,eeSalary,salType);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateSideLink(userRole);
	homepageAction.userLogout();
}


}