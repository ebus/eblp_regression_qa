package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GeneralInquiries_Other extends TestBase {
	public GeneralInquiries_Other() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : General Inquiries - Other
	  * Purpose :  To verify the Sub category - Other when Request Type is General Inquiries
	  * History : Created by Saivenkat Korivi on 03/25/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getGeneralInquiriesOther(){
		Object data[][] = TestUtil.getTestData("General Inquiries_Other");
		return data;		
}
	@Test(priority=1,dataProvider="getGeneralInquiriesOther")
	public void GeneralInquiriesOther(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
   		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String Note,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying General Inquiries - Other for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
       throw new SkipException("Verifying General Inquiries - Other for "+userRole + " is Skipped");
   }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateGeneralInquiriesOther(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,Note,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("pdfFilePath"),
			prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
	}
}
