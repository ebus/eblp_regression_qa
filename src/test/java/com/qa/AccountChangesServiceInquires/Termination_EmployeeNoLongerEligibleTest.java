package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Termination_EmployeeNoLongerEligibleTest extends TestBase {
	public Termination_EmployeeNoLongerEligibleTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Termination_Employee No Longer Eligible
	  * Purpose :  To verify the Sub category Employee no longer eligible when Request Type is Termination
	  * History : Created by Saivenkat Korivi on 03/14/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	PolicyBillingDetailPageAction policyBillingDetailPageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getEmployeeNoLongerEligible(){
		Object data[][] = TestUtil.getTestData("Employee No Longer Eligible");
		return data;		
}
	@Test(priority=1,dataProvider="getEmployeeNoLongerEligible")
	public void EmployeeNoLongerEligible(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,String eeSSN,String RsnForTermination,String LastDateWorked,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Employee No Longer Eligible for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Employee No Longer Eligible  for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEmployeeNoLongerEligible(policyHolderName, policyHolderNumber,
			RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,eeLastName,eeSSN,RsnForTermination,LastDateWorked);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	homepageAction.userLogout();
	
	}
}
