package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GeneralInquiries_IBillAccessTest extends TestBase {
	public GeneralInquiries_IBillAccessTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : General Inquiries - IBill Access
	  * Purpose :  To verify the Sub category - IBill Access when Request Type is General Inquiries
	  * History : Created by Saivenkat Korivi on 03/24/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getIBillAccess(){
		Object data[][] = TestUtil.getTestData("iBill Access");
		return data;		
}
	@Test(priority=1,dataProvider="getIBillAccess")
	public void IBillAccess(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String NameOfEmp, String Note,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying IBill Access for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying IBill Access for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateIBillAccess(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,NameOfEmp,Note,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("pdfFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateIBillAccess(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,NameOfEmp,Note,prop.getProperty("csvFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("docFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
	}
	
}
