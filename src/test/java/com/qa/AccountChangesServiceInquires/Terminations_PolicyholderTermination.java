package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Terminations_PolicyholderTermination extends TestBase{
	public Terminations_PolicyholderTermination() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Terminations_Policyholder Termination
	  * Purpose :  To verify the Sub category - Policyholder Termination when Request Type is Terminations
	  * History : Created by Saivenkat Korivi on 03/16/2022 	 
**************************************************************************************/	
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getPolicyholderTermination(){
		Object data[][] = TestUtil.getTestData("Policyholder Termination");
		return data;		
}
	@Test(priority=1,dataProvider="getPolicyholderTermination")
	public void PolicyholderTermination(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Policyholder Termination for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Policyholder Termination  for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validatePolicyholderTerminationSet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,
			prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("PNGFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validatePolicyholderTerminationSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("txtFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.userLogout();
	}

	
}
