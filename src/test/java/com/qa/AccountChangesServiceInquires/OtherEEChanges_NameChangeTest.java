package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OtherEEChanges_NameChangeTest extends TestBase {
	public OtherEEChanges_NameChangeTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Other Employee Changes - Name Change
	  * Purpose :  To verify the Sub category - Name Change when Request Type is Other Employee Changes
	  * History : Created by Saivenkat Korivi on 03/18/2022 	 
**************************************************************************************/	
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getNameChange(){
		Object data[][] = TestUtil.getTestData("Other EE Changes - Name Change");
		return data;		
}
	@Test(priority=1,dataProvider="getNameChange")
	public void NameChange(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeCurntFirstName,String eeCurntLastName,String eeSSN,String eeNewFirstName,String eeNewLastName,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Name Change for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Name Change for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateNameChange(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeCurntFirstName,eeCurntLastName,eeSSN,eeNewFirstName,eeNewLastName);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.userLogout();
	}
}