package com.qa.AccountChangesServiceInquires;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Enrollments_AddaSpouseChildTest extends TestBase{
	public Enrollments_AddaSpouseChildTest() {
		super();	
	}
	
/* ******************************************************************************
	  * Test Name : Enrollments_Add a Spouse Child
	  * Purpose : To validate whether user is able to submit Enrollments_Add a Spouse Child
	  * History : Created by Anjali Johny on 12/31/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getEnrollmentsAddSpouse(){
	Object data[][] = TestUtil.getTestData("Enrollments_Add a Spouse Child");
	return data;
}
@Test(priority=1,dataProvider="getEnrollmentsAddSpouse")
    public void EnrollmentsAddSpouse(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,
    		String eeSSN,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Enrollments_Add a Spouse Child for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Enrollments_Add a Spouse Child for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddSpouseSet1(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,
			eeLastName,eeSSN,prop.getProperty("docFilePath"),prop.getProperty("txtExceedSizePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddSpouseSet2(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,
			eeLastName,eeSSN,prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddSpouseSet3(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,
			eeLastName,eeSSN,prop.getProperty("pdfFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddSpouseSet4(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,
			eeLastName,eeSSN,prop.getProperty("txtFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAddSpouseSet5(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,
			eeLastName,eeSSN,prop.getProperty("csvFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	homepageAction.userLogout();
}


}