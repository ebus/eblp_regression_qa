package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class BillingInquiries_SubmitSummaryBillTest extends TestBase {
	public BillingInquiries_SubmitSummaryBillTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Billing Inquiries - Submit Summary Bill(s)
	  * Purpose :  To verify the Sub category - Submit Summary Bill(s) when Request Type is Billing Inquiries
	  * History : Created by Saivenkat Korivi on 03/22/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getSubmitSummaryBill(){
		Object data[][] = TestUtil.getTestData("Submit Summary Bill(s)");
		return data;		
}
	@Test(priority=1,dataProvider="getSubmitSummaryBill")
	public void SubmitSummaryBill(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String Note,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Billing Inquiries - Submit Summary Bill for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Billing Inquiries - Submit Summary Bill for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
//File upload Combination 1
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSubmitSummaryBillSet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("txtFilePath"),
			prop.getProperty("xlsxFilePath"),prop.getProperty("pdfFilePath"), Note);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File Upload Combination 2
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSubmitSummaryBillSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("xlsxFilePath"),Note);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File upload Combination 3
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSubmitSummaryBillSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("csvFilePath"),prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),Note);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File upload Combination 4
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSubmitSummaryBillSet2(policyHolderName,
				policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("xlsxFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("docFilePath"),Note);
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File upload Combination 5
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateSubmitSummaryBillSet2(policyHolderName,
						policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("pdfFilePath"),prop.getProperty("pdfFilePath"),prop.getProperty("csvFilePath"),Note);
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();

		homepageAction.userLogout();
	}
}
