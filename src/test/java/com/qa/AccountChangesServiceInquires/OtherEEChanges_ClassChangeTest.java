package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class OtherEEChanges_ClassChangeTest extends TestBase {
	public OtherEEChanges_ClassChangeTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Other Employee Changes - Class Change
	  * Purpose :  To verify the Sub category - Class Change when Request Type is Other Employee Changes
	  * History : Created by Saivenkat Korivi on 03/17/2022 	 
**************************************************************************************/	
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getClassChange(){
		Object data[][] = TestUtil.getTestData("Other EE Changes - Class Change");
		return data;		
}

	@Test(priority=1,dataProvider="getClassChange")
	public void ClassChange(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,String eeSSN,String ClassType1, String CurrentClass1,String NewClass1,
    		String ClassType2,String CurrentClass2,String NewClass2,String ClassType3,String CurrentClass3,String NewClass3,String ClassType4,String CurrentClass4,String NewClass4,String EffDateOfChange,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Class Change for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Class Change  for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateClassChange(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry, eeFirstName,eeLastName,eeSSN,ClassType1,CurrentClass1,NewClass1,ClassType2,CurrentClass2,NewClass2,ClassType3,CurrentClass3,NewClass3,ClassType4,
			CurrentClass4,NewClass4,EffDateOfChange);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
}
}