package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GroupDisabilityLifeClaims_LifeClaimTest extends TestBase {
	public GroupDisabilityLifeClaims_LifeClaimTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : Group Disability and Life Claims - Life Claim
	  * Purpose :  To verify the Sub category - Life Claim when Request Type is Group Disability and Life Claims
	  * History : Created by Saivenkat Korivi on 03/29/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getLifeClaim(){
		Object data[][] = TestUtil.getTestData("Life Claim");
		return data;		
}
	@Test(priority=1,dataProvider="getLifeClaim")
	public void LifeClaim(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
   		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Group Disability and Life Claims - Life Claim for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
       throw new SkipException("Verifying Group Disability and Life Claims - Life Claim for "+userRole + " is Skipped");
   }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateLifeClaim(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry);
	
	homepageAction.userLogout();
	}
}
