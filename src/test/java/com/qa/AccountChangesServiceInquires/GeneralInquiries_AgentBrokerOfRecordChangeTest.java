package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GeneralInquiries_AgentBrokerOfRecordChangeTest extends TestBase {
	public GeneralInquiries_AgentBrokerOfRecordChangeTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : General Inquiries - Agent/Broker of Record Change
	  * Purpose :  To verify the Sub category - Agent/Broker of Record Change when Request Type is General Inquiries
	  * History : Created by Saivenkat Korivi on 03/25/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getAgentBrokerOfRecordChange(){
		Object data[][] = TestUtil.getTestData("Agent_Broker of Record Change");
		return data;		
}
	@Test(priority=1,dataProvider="getAgentBrokerOfRecordChange")
	public void AgentBrokerOfRecordChange(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
   		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying General Inquiries - Agent/Broker of Record Change for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
       throw new SkipException("Verifying General Inquiries - Agent/Broker of Record Change for "+userRole + " is Skipped");
   }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
//file upload combination 1	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAgentBrokerOfRecordChangeSet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("PNGFilePath"),prop.getProperty("txtExceedSizePath"),prop.getProperty("txtFilePath"),
			prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File Upload Combination 2
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAgentBrokerOfRecordChangeSet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("pdfFilePath"),
			prop.getProperty("docFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
//File Upload Combination 3
		homepageAction.ValidateAccountChangeSideLink(userRole);
		accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateAgentBrokerOfRecordChangeSet2(policyHolderName,
				policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,prop.getProperty("csvFilePath"),
				prop.getProperty("pdfFilePath"));
		confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
		homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
		homepageAction.userLogout();
	}
}
