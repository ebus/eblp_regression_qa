package com.qa.AccountChangesServiceInquires;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Enrollments_EmployeePaidVoluntaryEETest extends TestBase{
	public Enrollments_EmployeePaidVoluntaryEETest() {
		super();	
	}
	
/* ******************************************************************************
	  * Test Name : Enrollments_EmployeePaidVoluntary EE
	  * Purpose : To validate whether user is able to submit Enrollments_EmployeePaidVoluntary EE
	  * History : Created by Anjali Johny on 12/24/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getEnrollmentsEmployerPaidVol(){
	Object data[][] = TestUtil.getTestData("Employee Paid Voluntary");
	return data;
}
@Test(priority=1,dataProvider="getEnrollmentsEmployerPaidVol")
    public void EnrollmentsEmployerPaidVol(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Employee Paid Voluntary EE for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Employee Paid Voluntary EE for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEmployeePaidVolEESet1(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("txtExceedSizePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEmployeePaidVolEESet2(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"),prop.getProperty("pdfFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEmployeePaidVolEESet3(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry
			,prop.getProperty("txtFilePath"),prop.getProperty("csvFilePath"),prop.getProperty("xlsxFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateAccountChangeSideLink(userRole);
	homepageAction.userLogout();
}


}