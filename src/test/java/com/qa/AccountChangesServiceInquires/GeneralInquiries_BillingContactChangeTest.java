package com.qa.AccountChangesServiceInquires;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class GeneralInquiries_BillingContactChangeTest extends TestBase {
	public GeneralInquiries_BillingContactChangeTest() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : General Inquiries - Blank Contact Change
	  * Purpose :  To verify the Sub category - Blank Contact Change when Request Type is General Inquiries
	  * History : Created by Saivenkat Korivi on 03/24/2022 	 
**************************************************************************************/
	LoginPageAction loginPageAction;
	HomePageAction homepageAction;
	AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
	AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
	ConfirmationPageAction confirmationPageAction;
	GenericFunction genericFunction = new GenericFunction();
	@DataProvider
	public Object[][] getBillingContactChange(){
		Object data[][] = TestUtil.getTestData("Billing Contact Change");
		return data;		
}
	@Test(priority=1,dataProvider="getBillingContactChange")
	public void BillingContactChange(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String NewContactFName,String NewContactLName,String NewContactEmail,String NewContactPhone, String EffectiveDate,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Billing Contact Change for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Billing Contact Change for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateBillingContactChange(policyHolderName,
			policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,NewContactFName,NewContactLName,NewContactEmail,NewContactPhone,EffectiveDate);
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	
	homepageAction.userLogout();
	}
}
