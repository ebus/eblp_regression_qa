package com.qa.AccountChangesServiceInquires;


import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.qa.base.TestBase;
import com.qa.pageActions.AccountChangesServiceInquiresPageAction;
import com.qa.pageActions.AccountChangesServiceInquiresReviewPageAction;
import com.qa.pageActions.ConfirmationPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;
import com.qa.pageActions.PolicyBillingDetailPageAction;
import com.qa.pageActions.PolicyBillingDetailsReviewPageAction;
import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class Enrollments_EmployerPaidTest extends TestBase{
	public Enrollments_EmployerPaidTest() {
		super();	
	}
	
/* ******************************************************************************
	  * Test Name : Enrollments_Employer Paid
	  * Purpose : To validate whether user is able to submit Enrollments_Employer Paid
	  * History : Created by Anjali Johny on 12/09/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homepageAction;
PolicyBillingDetailPageAction policyBillingDetailPageAction;
AccountChangesServiceInquiresPageAction accountChangesServiceInquiresPageAction;
AccountChangesServiceInquiresReviewPageAction accountChangesServiceInquiresReviewPageAction;
PolicyBillingDetailsReviewPageAction policyBillingDetailsReviewPageAction;
ConfirmationPageAction confirmationPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getEnrollmentsEmployerPaid(){
	Object data[][] = TestUtil.getTestData("Enrollments_Employer Paid");
	return data;
}
@Test(priority=1,dataProvider="getEnrollmentsEmployerPaid")
    public void EnrollmentsEmployerPaid(String userId, String passWord,String policyHolderName,String policyHolderNumber,String RequesterType,
    		String FirstName,String LastName,String Email,String Phone,String rqstType,String subCatgry,String eeFirstName,String eeLastName,
    		String eeDOB,String eeSSN,String eeDOH,String eeGender,String eeMartialStatus,String eeSalary,String salType,String eeOccupation,
    		String eeClassCovg,String baseLife,String accidentDD,String STD,String LTD,String userRole,String indExecute) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying Enrollments_Employer Paid for "+userRole);
	if (indExecute.equalsIgnoreCase("No")) {
        throw new SkipException("Verifying Enrollments_Employer Paid for "+userRole + " is Skipped");
    }
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homepageAction=loginPageAction.userLogin(userId, passWord);
	accountChangesServiceInquiresPageAction=homepageAction.validateAccountChangesLeftNav(userRole);
	accountChangesServiceInquiresReviewPageAction=accountChangesServiceInquiresPageAction.validateEmployeePaid(policyHolderName,policyHolderNumber,RequesterType,FirstName,LastName,Email,Phone,rqstType,subCatgry,eeFirstName,eeLastName,
			eeDOB,eeSSN,eeDOH,eeGender,eeMartialStatus,eeSalary,salType,eeOccupation,eeClassCovg,baseLife,accidentDD,STD,LTD
			,prop.getProperty("pdfFilePath"),prop.getProperty("docFilePath"),prop.getProperty("txtFilePath"));
	confirmationPageAction=accountChangesServiceInquiresReviewPageAction.validateReviewPage();
	homepageAction=confirmationPageAction.validateAccountChangesConfirmation();
	homepageAction.ValidateSideLink(userRole);
	homepageAction.userLogout();
}


}