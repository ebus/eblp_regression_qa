package com.qa.page;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class AccountChangesServiceInquiresReviewPage extends GenericFunction{

	@FindBy(id="edit")
	public WebElement btnEdit;
	@FindBy(id="submit")
	public WebElement btnSubmit;
	@FindBy(xpath="//h3[contains(text(),'Type of Request')]")
	public WebElement headerTypeOfRqst;
	@FindBy(xpath="//h3[contains(text(),'Policy Information')]")
	public WebElement headerPolicyInfo;
	@FindBy(xpath="//h3[contains(text(),'Requester Information')]")
	public WebElement headerRequesterInfo;
	
	
	public AccountChangesServiceInquiresReviewPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
