package com.qa.page;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class PolicyBillingDetailPage extends GenericFunction{

	
	//Policy Information
	@FindBy(xpath="//h3[contains(text(),'Policy Information')]")
	public WebElement headerPolicyInformation;
	@FindBy(id="policyHolderName")
	public WebElement txtPolicyHolderName;
	@FindBy(id="policyHolderNumber")
	public WebElement txtPolicyHolderNumber;
	
	//Requester Information
	@FindBy(xpath="//h3[contains(text(),'Requester Information')]")
	public WebElement headerRequesterInfo;
	@FindBy(id="requesterType")
	public WebElement dropdownRequesterType;
	@FindBy(id="firstName")
	public WebElement txtFirstName;
	@FindBy(id="lastName")
	public WebElement txtLastName;
	@FindBy(id="emailAddress")
	public WebElement txtEmail;
	@FindBy(id="phoneNumber")
	public WebElement txtPhone;
	
	//Type of Request
	@FindBy(xpath="//h3[contains(text(),'Type of Request')]")
	public WebElement headerTypeOfRequest;
	@FindBy(id="effectiveDate")
	public WebElement txtDate;
	@FindBy(id="activeClaim")
	public WebElement chckboxActiveClaim;
	@FindBy(xpath="//label[contains(text(),'  Select All')]/preceding-sibling::node()")
	public WebElement chckboxSelectAll;
	@FindBy(xpath="//input[@value='Life']")
	public WebElement chckboxLife;
	@FindBy(xpath="//input[@value='ADD – Accidental Death & Dismemberment']")
	public WebElement chckboxADD;
	@FindBy(xpath="//input[@value='DLF – Dependent Life']")
	public WebElement chckboxDLF;
	@FindBy(xpath="//input[@value='DAD – Dependent ADD']")
	public WebElement chckboxDAD;
	@FindBy(xpath="//input[@value='VTL – Voluntary Term Life']")
	public WebElement chckboxVTL;
	@FindBy(xpath="//input[@value='VTA – Voluntary ADD']")
	public WebElement chckboxVTA;
	@FindBy(xpath="//input[@value='DVT – Voluntary Dependent Term Life']")
	public WebElement chckboxDVT;
	@FindBy(xpath="//input[@value='DVA – Voluntary Dependent ADD']")
	public WebElement chckboxDVA;
	@FindBy(xpath="//input[@value='STD – Short Term Disability']")
	public WebElement chckboxSTD;
	@FindBy(xpath="//input[@value='LTD – Long Term Disability']")
	public WebElement chckboxLTD;
	@FindBy(xpath="//input[@value='WDS – Worksite Disability Short Term']")
	public WebElement chckboxWDS;
	@FindBy(xpath="//input[@value='WDL – Worksite Disability Long Term']")
	public WebElement chckboxWDL;
	@FindBy(xpath="//input[@value='OLS -  One Lump Sum']")
	public WebElement chckboxOLS;
	@FindBy(xpath="//div[@class='form-group clearfix']//input[@name='classNumbers1']")
    public WebElement txtClassNumber;
	@FindBy(id="changeRequested")
	public WebElement dropdownChangeRqsted;
	@FindBy(id="comments")
	public WebElement txtComments;
	@FindBy(xpath="//div[@class='col-sm-offset-4 col-sm-5 col-xs-12']//button[contains(text(),'Continue')]")
	public WebElement btnContinue;
	
	//Account Changes
	
	@FindBy(id="requestType")
	public WebElement dropdownRqstType;
	@FindBy(id="subCategory")
	public WebElement dropdownSubCategory;
	@FindBy(id="employeeFirstName")
	public WebElement txteeFname;
	@FindBy(id="employeeLastName")
	public WebElement txteeLname;
	@FindBy(id="employeeDateOfBirth")
	public WebElement txteeDOB;
	@FindBy(id="employeeSsn")
	public WebElement txteeSSN;
	@FindBy(id="employeeDateOfHire")
	public WebElement txteeDOH;
	@FindBy(id="employeeGender")
	public WebElement txteeGender;
	@FindBy(id="employeeMaritalStatus")
	public WebElement txteeMaritalStatus;
	@FindBy(id="employeeSalary")
	public WebElement txteeSalary;
	@FindBy(id="employeeSalaryType")
	public WebElement txteeSalaryTyp;
	@FindBy(id="employeeOccupation")
	public WebElement txteeOccupation;
	@FindBy(id="employeeClassOfCoverage")
	public WebElement txteeClassCovg;
	@FindBy(xpath="//input[@value='Basic Life']")
	public WebElement chckboxBaseLife;
	@FindBy(xpath="//input[@value='Accidental Death & Dismemberment']")
   public WebElement  chckboxAccidentDD;
	@FindBy(xpath="//input[@value='Short-Term Disability']")
	public WebElement chckboxAcctChangeSTD;
	@FindBy(xpath="//input[@value='Long-Term Disability']")
   public WebElement  chckboxAccountChangeLTD;
	
	
	
	
	//file upload
	//@FindBy(xpath="(//input[@type='file'])[1]")
	@FindBy(id="policyHolderRequestForm")
	public WebElement filePolicy;
	@FindBy(id="policyHolderRequestForm2")
	public WebElement filePolicy2;
	@FindBy(id="policyHolderRequestForm_error")
	public WebElement filePolicyError;
	@FindBy(id="policyHolderRequestForm2_error")
	public WebElement filePolicyError2;
	@FindBy(id="groupChangeRequestForm_error")
	public WebElement fileChangeRequesrForm_Error;
	@FindBy(id="adminOptionsElectionForm_error")
	public WebElement fileAdminError;
	@FindBy(id="proposalAttachment_error")
	public WebElement fileProposalError;
	@FindBy(id="censusAttachment_error")
	public WebElement fileCensusError;
	@FindBy(id="optionalFile1_error")
	public WebElement fileOptionError;
	@FindBy(xpath="//input[@id='optionalFile1']")
	public WebElement fileOption1;
	@FindBy(id="eapElectionForm_error")
	public WebElement fileEOP_error;
	@FindBy(xpath="//input[@id='optionalFile2']")
	public WebElement fileOption2;
	@FindBy(id="optionalFile3")
	public WebElement fileOption3;
	@FindBy(id="optionalFile4")
	public WebElement fileOption4;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='adminOptionsElectionForm']")
	public WebElement fileAdminOption;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='censusAttachment']")
	public WebElement fileCensus;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='proposalAttachment']")
	public WebElement fileProposal;
	@FindBy(xpath="(//label[contains(text(),'Add File')])[1]")
	public WebElement btnAddFile;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='policyHolderRequestForm']")
	public WebElement fileUploadPolicy;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='policyHolderRequestForm2']")
	public WebElement fileUploadPolicy2;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='groupChangeRequestForm']")
	public WebElement fileUploadChangeRequestForm;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='adminOptionsElectionForm']")
	public WebElement fileAdminOptionsElectionForm;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='eapElectionForm']")
	public WebElement fileEOP;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='optionalFile1']")
	public WebElement fileUploadOption1;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='optionalFile2']")
	public WebElement fileUploadOption2;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='optionalFile3']")
	public WebElement fileUploadOption3;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//label[@for='optionalFile4']")
	public WebElement fileUploadOption4;
	
	public PolicyBillingDetailPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
