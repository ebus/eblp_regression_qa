package com.qa.page;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HomePage extends GenericFunction{

	@FindBy(xpath="//span[contains(text(),'Our Plans')]")
	public WebElement linkOurPlans;
	@FindBy(xpath="//div[@id='breadcrumbs']//a[contains(text(),'Home')]")
	public WebElement linkHome;
	@FindBy(xpath="//ul//a[@href='#policy-changes-form']")
	public WebElement sublinkPolicyBillingChanges;
	@FindBy(xpath="//ul//a[@href='#employee-account-changes']")
	public WebElement sublinkAccountServiceInquiresChanges;
	@FindBy(xpath="//a[@class='dropdown-toggle' and @href='#']/i[@class='ace-icon fa fa-caret-down']")
	public WebElement linkLogoutMenu;
	@FindBy(xpath="//span[contains(text(),'My Employees')]")
	public WebElement linkMyEmp;
	@FindBy(xpath="//span[contains(text(),'My Business')]")
	public WebElement linkMyBusiness;
	@FindBy(xpath="//li/a[@href='/public/logout.html']")
	public WebElement linkLogout;
	@FindBy(xpath="//a[@href='#employee-account-changes']")
	public WebElement linkAccountChanges;
	public HomePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
