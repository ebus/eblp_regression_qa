package com.qa.page;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class ConfirmationPage extends GenericFunction{

	@FindBy(xpath="//h3[contains(text(),'Success')]")
	public WebElement headerSuccess;
    @FindBy(id="makeNewRequest")
    public WebElement btnMakeNewRequest;
	@FindBy(xpath="//div[@id='policy-changes-form-container']//p")
	public WebElement txtMessage;
	@FindBy(xpath = "//div[@id='policy-changes-form-container']//p[2]")
	public WebElement txtPolicyChangesThankyouMsg;
	
	@FindBy(xpath="//div[@id='form-container']//p")
	public WebElement txtAccountChnagesMessage;
	@FindBy(xpath="//div[@id='form-container']//p[2]")
	public WebElement txtAccountChangesThankyouMessage;
	
	@FindBy(xpath="//h3[contains(text(),'Error')]")
	public WebElement headerError;
	public ConfirmationPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
