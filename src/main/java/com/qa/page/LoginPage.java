package com.qa.page;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class LoginPage extends GenericFunction{

	@FindBy(id="username2")
	public WebElement txtUserName;
	
	@FindBy(id="password2")
	public WebElement txtPassword;
	
	@FindBy(xpath="//button[@class='btn-u js-contentapp-login-submit']")
	public WebElement btnLogin;
	
	@FindBy(xpath="//a[contains(text(),'Login')]")
	public WebElement btn_Login;
	
	@FindBy(id= "oktaSignIn2")
	public WebElement btn_OALogin;
	
	
	public LoginPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}
