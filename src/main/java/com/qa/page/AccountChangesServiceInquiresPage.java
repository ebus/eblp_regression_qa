package com.qa.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class AccountChangesServiceInquiresPage extends GenericFunction{

	//Policy Information
	@FindBy(xpath="//h3[contains(text(),'Policy Information')]")
	public WebElement headerPolicyInformation;
	@FindBy(id="policyHolderName")
	public WebElement txtPolicyHolderName;
	@FindBy(id="policyHolderNumber")
	public WebElement txtPolicyHolderNumber;
	
	//Requester Information
	@FindBy(xpath="//h3[contains(text(),'Requester Information')]")
	public WebElement headerRequesterInfo;
	@FindBy(id="requesterType")
	public WebElement dropdownRequesterType;
	@FindBy(id="firstName")
	public WebElement txtFirstName;
	@FindBy(id="lastName")
	public WebElement txtLastName;
	@FindBy(id="emailAddress")
	public WebElement txtEmail;
	@FindBy(id="phoneNumber")
	public WebElement txtPhone;
	
	//Type of Request
	@FindBy(xpath="//h3[contains(text(),'Type of Request')]")
	public WebElement headerTypeOfRequest;
	@FindBy(id="requestType")
	public  WebElement dropdownRequestTyp;
	@FindBy(id="subCategory")
	public WebElement dropdownSubCategory;
	@FindBy(id="employeeFirstName")
	public WebElement txtEEfname;
	@FindBy(id="employeeLastName")
	public WebElement txtEELstname;
	@FindBy(id="employeeDateOfBirth")
	public WebElement txtDOB;
	@FindBy(id="employeeSsn")
	public WebElement txtSSN;
	@FindBy(id="employeeDateOfHire")
	public WebElement txtDOH;
	@FindBy(id="employeeGender")
	public WebElement dropdownGender;
	@FindBy(id="employeeMaritalStatus")
	public WebElement dropdownMstatus;
	@FindBy(id="employeeSalary")
	public WebElement txtEEsalary;
	@FindBy(id="employeeSalaryType")
	public WebElement dropdownSaltype;
	@FindBy(id="employeeOccupation")
	public WebElement txtEEoccupation;
	@FindBy(id="employeeClassOfCoverage")
	public WebElement txtEEclassCovg;
	@FindBy(xpath="//input[@value='Basic Life']")
	public WebElement chckboxBaseLife;
	@FindBy(xpath="//input[@value='Accidental Death & Dismemberment']")
    public WebElement chckboxADD;
	@FindBy(xpath="//input[@value='Short-Term Disability']")
	public WebElement chckboxSTD;
	@FindBy(xpath="//input[@value='Long-Term Disability']")
	public WebElement chckboxLTD;
	@FindBy(xpath="//div[@class='col-sm-offset-4 col-sm-5 col-xs-12']//button[contains(text(),'Continue')]")
	public WebElement btnContinue;
	@FindBy (id="rehireDate")
	public WebElement txtRehireDate;
	@FindBy (id="terminationReason")
	public WebElement dropdownTerminationReason;
	@FindBy (id="lastDateWorked")
	public WebElement eeLastDateWorked;
	@FindBy (id="terminationDate")
	public WebElement eeTerminationDate;
	@FindBy (id="effectiveDateOfChange")
	public WebElement effDateOfChange;
	
	//Other EE Changes - Class Change
	@FindBy (xpath="//a[@class=\"js-addCoverageLink noDecoration\"]")
	public WebElement AddCoverageBtn;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[1]/td[1]/select")
	public WebElement classType1;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[1]/td[2]/input")
	public WebElement currentClass1;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[1]/td[3]/input")
	public WebElement newClass1;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[2]/td[1]/select")
	public WebElement classType2;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[2]/td[2]/input")
	public WebElement currentClass2;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[2]/td[3]/input")
	public WebElement newClass2;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[3]/td[1]/select")
	public WebElement classType3;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[3]/td[2]/input")
	public WebElement currentClass3;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[3]/td[3]/input")
	public WebElement newClass3;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[4]/td[1]/select")
	public WebElement classType4;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[4]/td[2]/input")
	public WebElement currentClass4;
	@FindBy (xpath="//table[@id='classTypeDynamicInputTable']//tbody/tr[4]/td[3]/input")
	public WebElement newClass4;
	
	//SalaryChange
	@FindBy (id="employeeNewSalary")
	public WebElement txtNewSalary;
	@FindBy (id="employeeSalaryType")
	public WebElement dropdownSalaryType;
	@FindBy (id="employeeNewSalaryEffectiveDate")
	public WebElement txtNewSalaryEffDate;
	
	//Name Change
	@FindBy (id="employeeNewFirstName")
	public WebElement txteeNewFName;
	@FindBy (id="employeeNewLastName")
	public WebElement txteeNewLName;
	
	//Address Change
	@FindBy (id="employeeNewAddress")
	public WebElement txteeNewAddress;
	@FindBy (id="employeeNewCity")
	public WebElement txteeNewCity;
	@FindBy (id="employeeNewState")
	public WebElement txteeNewState;
	@FindBy (id="employeeNewZipCode")
	public WebElement txteeNewZipCode;
	
	//Submit Summary Bill
	@FindBy (xpath="//div[@class=\"col-sm-5 col-xs-12\"]/textarea")
	public WebElement txtNotes;
	
	//Notification of Payment
	@FindBy (id="paymentAmount")
	public WebElement txtPayAmount;
	
	//Where is my bill?, Copy of bill, Why did my Bill change? and How Do I?
	@FindBy (xpath="//p[@class=\"col-sm-5 col-xs-12\"]")
	public WebElement txtBillMessage;
	@FindBy (id="billingMonthYear")
	public WebElement txtMonthYear;
	
	//Billing Contact Change
	@FindBy (id="newContactFirstName")
	public WebElement NewContactFN;
	@FindBy (id="newContactLastName")
	public WebElement NewContactLN;
	@FindBy (id="newContactEmail")
	public WebElement NewContactEmail;
	@FindBy (id="newContactPhone")
	public WebElement NewContactPhone;
	@FindBy (id="newContactEffectiveDate")
	public WebElement EffectiveDate;
	
	//IBill Access
	@FindBy (id="employeeFullName")
	public WebElement txtNameOfEE;
	
	//Cert/Policy Copies
	@FindBy (xpath="//p[@class=\"col-sm-5 col-xs-12\"]")
	public WebElement InfoMsg;
	
	//file Upload
	@FindBy(xpath="//label[@for='enrollmentForm']")
	public WebElement fileBtnEnrollmentForm;
	@FindBy(id="enrollmentForm_error")
	public WebElement fileErrorMsgEnrollForm;
	@FindBy(id="optionalFile1_error")
	public WebElement fileErrorMsgOption1;
	@FindBy(id="optionalFile2_error")
	public WebElement fileErrorMsgOption2;
	@FindBy(xpath="//label[@for='optionalFile1']")
	public WebElement fileBtnOption1;
	@FindBy(xpath="//label[@for='optionalFile2']")
	public WebElement fileBtnOption2;
	@FindBy(xpath="//label[@for='eoiForm']")
	public WebElement fileBtnEoiForm;
	@FindBy(id="eoiForm_error")
	public WebElement fileErrorMsgEoiForm;
	@FindBy(id="employeeRfcForm_error")
	public WebElement fileRfcError;
	@FindBy(xpath="//label[@for='employeeRfcForm']")
	public WebElement fileEmployeeRfcform;
	@FindBy(id="employeeRfcForm_error")
	public WebElement fileErrorMesgRfcForm;
	@FindBy(xpath="//label[@for='policyHolderTerminationForm']")
	public WebElement fileAttachBtn;
	@FindBy(id="policyHolderTerminationForm_error")
	public WebElement fileAttachError;
	@FindBy(id="optionalFile2_error")
	public WebElement FileTypeError;
	@FindBy(xpath="//label[@for='censusForm']")
	public WebElement fileCensusForm;
	@FindBy(id="censusForm_error")
	public WebElement fileTypeCensusFormErr;
	@FindBy(xpath="//label[@for='optionalCensusForm']")
	public WebElement fileOptionalCensusForm;
	@FindBy (id="optionalCensusForm_error")
	public WebElement fileOptionalCensusFormErr;
	@FindBy(id="censusForm_error")
	public WebElement fileSizeExceedCensusFormError;
	@FindBy(id="optionalCensusForm_error")
	public WebElement fileSizeExceedOptionalCensusFormError;
	
	//Beneficiary
	@FindBy (xpath="//label[@for='beneficiaryForm']")
	public WebElement fileBeneficiaryform;
	@FindBy (id="beneficiaryForm_error")
	public WebElement fileBeneficiaryErr;
	
	//GIB
	@FindBy (xpath="//label[@for='guaranteedIncreaseAttachment1']")
	public WebElement fileGIBMandatory;
	@FindBy (xpath="//label[@for='guaranteedIncreaseAttachment2']")
	public WebElement fileGIBOption;
	@FindBy (id="guaranteedIncreaseAttachment1_error")
	public WebElement FileGIBMandatoryErr;
	@FindBy (id="guaranteedIncreaseAttachment2_error")
	public WebElement FileGIBOptionalErr;
	
	//Submit Summary Bill
	@FindBy (xpath="//label[@for='summaryBillsForm']")
	public WebElement fileSSBMandatory;
	@FindBy (id="summaryBillsForm_error")
	public WebElement fileSSBMandatoryErr;
	
	//Notification of payment
	@FindBy (xpath="//label[@for='notificationOfPaymentForm']")
	public WebElement fileNOPForm;
	@FindBy (id="notificationOfPaymentForm_error")
	public WebElement fileNOPErr;
	
	//Other
	@FindBy (xpath="//label[@for='otherForm']")
	public WebElement OtherBtn;
	@FindBy (id="otherForm_error")
	public WebElement fileOtherErr;
	//IBill Access
	@FindBy (xpath="//label[@for='adminOptionsElectionForm']")
	public WebElement fileIBillBtn;
	@FindBy (id="adminOptionsElectionForm_error")
	public WebElement fileIBillAccessErr;
	
	//Multiple Changes
	@FindBy (xpath="//label[@for='multipleChangesAttachment1']")
	public WebElement fileMultipleChangeAttach1;
	@FindBy (xpath="//label[@for='multipleChangesAttachment2']")
	public WebElement fileMultipleChangeAttach2;
	@FindBy (id="multipleChangesAttachment1_error")
	public WebElement fileMultipleChangesErr1;
	@FindBy (id="multipleChangesAttachment2_error")
	public WebElement fileMultipleChangesErr2;
	
	//Agent/Broker of Record Change
	@FindBy (xpath="//label[@for='recordChangeForm']")
	public WebElement fileABRecordChangeAttach1;
	@FindBy (xpath="//label[@for='optionalBrokerRecordChangeFile']")
	public WebElement fileABRecordChangeAttach2;
	@FindBy (id="recordChangeForm_error")
	public WebElement fileABRecordChangeErr;
	@FindBy (id="optionalBrokerRecordChangeFile_error")
	public WebElement fileABRecordChangeOptionalErr;
	
	
	public AccountChangesServiceInquiresPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}