package com.qa.pageActions;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;

import com.qa.page.AccountChangesServiceInquiresPage;
import com.qa.util.Wait;
import com.relevantcodes.extentreports.LogStatus;

public class AccountChangesServiceInquiresPageAction extends AccountChangesServiceInquiresPage {
	Wait wait = new Wait();
	public AccountChangesServiceInquiresPageAction() {
		super();
		
	}
	String errorMessage="File size exceeds 8MB size limit";
	String errorMessage10mb="File size exceeds 10MB size limit";
	String errorMessageFileType="The file is not an accepted file type";
	String errorMessage13mb="File size exceeds 13MB size limit";
		/* *****************************************************************************
		  * Test Name : AccountChangesServiceInquiresPageAction 
		  * Purpose : This class contains the action of  the AccountChangesServiceInquires page
		  * History : Created by Anjali Johny on 12/16/2021 
		  **************************************************************************************/
		
		public AccountChangesServiceInquiresReviewPageAction validateEmployeePaid(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
				     String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLstname,String eeDob,String eeSSN,
				     String doh,String eeGender,String mStatus,String eeSalary,String Saltyp,String eeOccupation,String eeCC,String life,
				     String add,String std,String ltd,String pdfFilePath,String docFilePath,String txtFilePath) throws Exception  {
			Thread.sleep(1000);
			
		
			//Policy Information Section
			assertText(headerPolicyInformation,"Policy Information","Policy Information header");
			EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
			EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
			takeScreenshot("Account Changes_PolicyInformationSection");
			//RequesterInformation Section
			assertText(headerRequesterInfo,"Requester Information","Requester Information header");
			ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
			EnterText(txtFirstName, firstName, "First Name");
			EnterText(txtLastName, lastName,"Last Name");
			EnterText(txtEmail,email,"Email");
			EnterText(txtPhone, phone,"Phone Number");
			takeScreenshot("Account Changes_RequesterInformation");
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		//	validateRequestType(requestTyp,subCatgry,eeFname,eeLstname,eeDob,eeSSN,doh,eeGender,mStatus,eeSalary,Saltyp,eeOccupation,eeCC,life,add,std,ltd,
			//		pdfFilePath,docFilePath,txtFilePath);
			Thread.sleep(2000);
			EnterText(txtEEfname,eeFname,"Employee First Name");
			EnterText(txtEELstname,eeLstname,"Employee Last Name");
			EnterText(txtDOB,eeDob,"Employee Date of Birth");
			EnterText(txtSSN, eeSSN,"Employee Social Security Number");
			EnterText(txtDOH,doh, "Employee Date of Hire");
			ComboSelectValue(dropdownGender,eeGender,"Employee Gender");
			takeScreenshot("Account Changes_Type of Request Down");
			//EnterText(dropdownGender,eeGender,"Employee Gender");
			ComboSelectValue(dropdownMstatus, mStatus, "Employee Marital Status");
			//EnterText(dropdownMstatus,mStatus,"Employee Marital Status");
			EnterText(txtEEsalary,eeSalary,"Employee Salary");
			ComboSelectValue(dropdownSaltype,Saltyp,"Salary Type");
			//EnterText(dropdownSaltype, Saltyp, "Salary Type");
			EnterText(txtEEoccupation,eeOccupation,"Employee Occupation");
			EnterText(txtEEclassCovg,eeCC,"Employee Class of Coverage");
			takeScreenshot("Account Changes_Class Coverage");
			selectCoverage(life,add,std,ltd);
			validateContinue();
		return new AccountChangesServiceInquiresReviewPageAction();
			
			}
		
		
		
		
		//Testcase Enrollments_Employee paid/Voluntary (EE)

		public AccountChangesServiceInquiresReviewPageAction validateEmployeePaidVolEESet1(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
				     String email,String phone,String requestTyp,String subCatgry,String pdfFilePath,String docFilePath,String txtFilePath,String txtExceedSizePath) throws Exception  {
			Thread.sleep(1000);
			
		
			//Policy Information Section
			takeScreenshot("Account Changes_PolicyInformationSection");
			assertText(headerPolicyInformation,"Policy Information","Policy Information header");
			EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
			EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
			
			//RequesterInformation Section
			assertText(headerRequesterInfo,"Requester Information","Requester Information header");
			ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
			EnterText(txtFirstName, firstName, "First Name");
			EnterText(txtLastName, lastName,"Last Name");
			EnterText(txtEmail,email,"Email");
			EnterText(txtPhone, phone,"Phone Number");
			takeScreenshot("Account Changes_RequesterInformation");
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);
			fileBtnEnrollmentForm.click();
			validateEnrollFormErrorMsg(txtExceedSizePath, subCatgry);
			//validateFileUpload(doc5mbsizePath, subCatgry);
			fileBtnEnrollmentForm.click();
			validateFileUpload(pdfFilePath,subCatgry);
			fileBtnOption1.click();
			validateOption1ErrorMsg(txtExceedSizePath, subCatgry);
			
			fileBtnOption1.click();
			validateFileUpload(docFilePath,subCatgry);
			fileBtnOption2.click();
			validateFileUpload(txtFilePath, subCatgry);
			validateContinue();
		return new AccountChangesServiceInquiresReviewPageAction();
			
			}
		public AccountChangesServiceInquiresReviewPageAction validateEmployeePaidVolEESet2(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String pdfFilePath,String docFilePath,String txtFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
	//	validateRequestType(requestTyp,subCatgry,eeFname,eeLstname,eeDob,eeSSN,doh,eeGender,mStatus,eeSalary,Saltyp,eeOccupation,eeCC,life,add,std,ltd,
		//		pdfFilePath,docFilePath,txtFilePath);
		Thread.sleep(2000);
		fileBtnEnrollmentForm.click();
		validateFileUpload(docFilePath,subCatgry);
		fileBtnOption1.click();
		validateFileUpload(txtFilePath,subCatgry);
		fileBtnOption2.click();
		validateFileUpload(pdfFilePath, subCatgry);
		validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		
		}
		public AccountChangesServiceInquiresReviewPageAction validateEmployeePaidVolEESet3(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String txtFilePath,String csvFilePath,String xlsxFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
		fileBtnEnrollmentForm.click();
		validateFileUpload(txtFilePath,subCatgry);
		fileBtnOption1.click();
		validateFileUpload(csvFilePath,subCatgry);
		fileBtnOption2.click();
		validateFileUpload(xlsxFilePath, subCatgry);
		validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		
		}
		

	//Testcase Enrollments_Medical Underwriting (EOI)
	
		public AccountChangesServiceInquiresReviewPageAction validateEnrollmentsMedicalUWSet1(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String docFilePath,String txtExceedSizePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
		fileBtnEoiForm.click();
	    validateFileSize10MBErrorMsg(fileErrorMsgEoiForm,txtExceedSizePath, subCatgry);
	    scrollIntoView(btnContinue, driver);
	    fileBtnEoiForm.click();
	    validateFileUpload(docFilePath,subCatgry);
	    validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		public AccountChangesServiceInquiresReviewPageAction validateEnrollmentsMedicalUWSet2(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String pdfFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
	    scrollIntoView(btnContinue, driver);
	    fileBtnEoiForm.click();
		validateFileUpload(pdfFilePath,subCatgry);
	    validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		
		public AccountChangesServiceInquiresReviewPageAction validateEnrollmentsMedicalUWSet3(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String txtFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
	    scrollIntoView(btnContinue, driver);
	    fileBtnEoiForm.click();
	    validateFileUpload(txtFilePath, subCatgry);
	    validateContinue();
	   return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		public AccountChangesServiceInquiresReviewPageAction validateEnrollmentsMedicalUWSet4(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String xlsxFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
	    scrollIntoView(btnContinue, driver);
	    fileBtnEoiForm.click();
	    validateFileUpload(xlsxFilePath, subCatgry);
	    validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		
		public AccountChangesServiceInquiresReviewPageAction validateEnrollmentsMedicalUWSet5(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String csvFilePath) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
		takeScreenshot("Account Changes_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
	    scrollIntoView(btnContinue, driver);
	    fileBtnEoiForm.click();
	    validateFileUpload(csvFilePath, subCatgry);
	    validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		
		
		
//Test case Enrollments_Reinstatement – Employer Paid
		
		public AccountChangesServiceInquiresReviewPageAction validateReinstatementEmployeePaid(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String requestTyp,String subCatgry,String rehireDate,String eeFname,String eeLstname,String eeDob,String eeSSN,
			     String eeSalary,String Saltyp) throws Exception  {
		Thread.sleep(1000);
		
	
		//Policy Information Section
	
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		takeScreenshot("Account Changes_PolicyInformationSection");
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Account Changes_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Account Changes_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
		ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
		ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
		Thread.sleep(2000);
		EnterText(txtRehireDate,rehireDate,"Rehire Date");
		EnterText(txtEEfname,eeFname,"Employee First Name");
		EnterText(txtEELstname,eeLstname,"Employee Last Name");
		EnterText(txtDOB,eeDob,"Employee Date of Birth");
		EnterText(txtSSN, eeSSN,"Employee Social Security Number");	
		takeScreenshot("Account Changes_Type of Request Down");
		EnterText(txtEEsalary,eeSalary,"Employee Salary");
		ComboSelectValue(dropdownSaltype,Saltyp,"Salary Type");
		validateContinue();
	return new AccountChangesServiceInquiresReviewPageAction();
}
		
//Enrollments_AddaSpouseChildTest
		public AccountChangesServiceInquiresReviewPageAction validateAddSpouseSet1(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String docFilePath,String txtExceedSizePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			Thread.sleep(2000);
			fileEmployeeRfcform.click();
			validateFileSizeErrorMsg(fileErrorMesgRfcForm,txtExceedSizePath, subCatgry);
		  //  scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(docFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
	
		public AccountChangesServiceInquiresReviewPageAction validateAddSpouseSet2(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String xlsxFilePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			Thread.sleep(2000);
		    scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(xlsxFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
		public AccountChangesServiceInquiresReviewPageAction validateAddSpouseSet3(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String pdfFilePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			Thread.sleep(2000);
			scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(pdfFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
		public AccountChangesServiceInquiresReviewPageAction validateAddSpouseSet4(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String txtFilePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			Thread.sleep(2000);
		    scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(txtFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
		public AccountChangesServiceInquiresReviewPageAction validateAddSpouseSet5(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String csvFilePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			Thread.sleep(2000);
			
		    scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(csvFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		
		//Terminations - Remove Spouse/Child
		public AccountChangesServiceInquiresReviewPageAction validatRemoveSpouseSet1(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN,String docFilePath,String txtExceedSizePath) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			
			Thread.sleep(2000);
			fileEmployeeRfcform.click();
			validateFileSizeErrorMsg(fileErrorMesgRfcForm,txtExceedSizePath, subCatgry);
		  //  scrollIntoView(btnContinue, driver);
		    fileEmployeeRfcform.click();
		    validateFileUpload(docFilePath,subCatgry);
		    validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
		}
		
		//Termination - Employee no longer Eligible
		public AccountChangesServiceInquiresReviewPageAction validateEmployeeNoLongerEligible(String policyHolderName,String policyHolderNumber,
				String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
				String eeFname,String eeLname,String eeSSN, String RsnForTermination, String LastDateWorked) throws Exception {
			
			validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
			
			//Type Of Request
			scrollIntoView(txtPhone, driver);
			takeScreenshot("Account Changes_Type of Request Up");
			assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
			ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
			ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
			Thread.sleep(2000);	
			EnterText(txtEEfname, eeFname, "EE First Name");
			EnterText(txtEELstname,eeLname,"EE Last Name");
			EnterText(txtSSN,eeSSN,"EE SSN");
			ComboSelectValue(dropdownTerminationReason,RsnForTermination,"Request Type");
			EnterText(eeLastDateWorked,LastDateWorked,"Request Type");
			validateContinue();
			
			return new AccountChangesServiceInquiresReviewPageAction();
			
		}
		
		//Termination - Employee Elects to Cancel
				public AccountChangesServiceInquiresReviewPageAction validateEmployeeElectsToCancelSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String TerminationDate,String pdfFilePath,String docFilePath,String txtFilePath,String txtExceedSizePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);	
					EnterText(eeTerminationDate,TerminationDate,"Request Type");
					fileEmployeeRfcform.click();
					validateFileSizeErrorMsg(fileErrorMesgRfcForm,txtExceedSizePath, subCatgry);
					fileEmployeeRfcform.click();
					validateFileUpload(pdfFilePath,subCatgry);
					fileBtnOption1.click();
					validateOption1ErrorMsg(txtExceedSizePath, subCatgry);
					fileBtnOption1.click();
					validateFileUpload(docFilePath,subCatgry);
					fileBtnOption2.click();
					validateFileUpload(txtFilePath, subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateEmployeeElectsToCancelSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String TerminationDate,String pdfFilePath,String docFilePath,String txtFilePath, String PNGFileType) throws Exception  {
				Thread.sleep(1000);
				
			
				validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
				
				//Type Of Request
				scrollIntoView(txtPhone, driver);
				takeScreenshot("Account Changes_Type of Request Up");
				assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
				ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
				ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
				Thread.sleep(2000);
				EnterText(eeTerminationDate,TerminationDate,"Request Type");
				fileEmployeeRfcform.click();
				validateFileTypeErrorMsg(fileErrorMesgRfcForm,PNGFileType,subCatgry);
				fileEmployeeRfcform.click();
				validateFileUpload(docFilePath,subCatgry);
				fileBtnOption1.click();
				validateFileUpload(txtFilePath,subCatgry);
				fileBtnOption2.click();
				validateFileUpload(pdfFilePath, subCatgry);
				validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
				
				}
				public AccountChangesServiceInquiresReviewPageAction validateEmployeeElectsToCancelSet3(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String TerminationDate,String txtFilePath,String csvFilePath,String xlsxFilePath) throws Exception  {
				Thread.sleep(1000);
				
			
				validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
				
				//Type Of Request
				scrollIntoView(txtPhone, driver);
				takeScreenshot("Account Changes_Type of Request Up");
				assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
				ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
				ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
				EnterText(eeTerminationDate,TerminationDate,"Request Type");
				Thread.sleep(2000);
				fileEmployeeRfcform.click();
				validateFileUpload(txtFilePath,subCatgry);
				fileBtnOption1.click();
				validateFileUpload(csvFilePath,subCatgry);
				fileBtnOption2.click();
				validateFileUpload(xlsxFilePath, subCatgry);
				validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
				
				}
		
	//Terminations - Policyholder Termination
				public AccountChangesServiceInquiresReviewPageAction validatePolicyholderTerminationSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String pdfFilePath,String docFilePath,String txtFilePath,String txtExceedSizePath, String PNGFileType) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);	
					fileAttachBtn.click();
					validateFileSizeErrorMsg(fileAttachError,txtExceedSizePath, subCatgry);
					fileAttachBtn.click();
					validateFileUpload(pdfFilePath,subCatgry);
					fileBtnOption1.click();
					validateOption1ErrorMsg(txtExceedSizePath, subCatgry);
					fileBtnOption1.click();
					validateFileUpload(docFilePath,subCatgry);
					fileBtnOption2.click();
					validateFileTypeOptionalErrorMsg(FileTypeError,PNGFileType,subCatgry);
					fileBtnOption2.click();
					validateFileUpload(txtFilePath, subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validatePolicyholderTerminationSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String txtFilePath,String csvFilePath,String xlsxFilePath) throws Exception  {
				Thread.sleep(1000);
				
			
				validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
				
				//Type Of Request
				scrollIntoView(txtPhone, driver);
				takeScreenshot("Account Changes_Type of Request Up");
				assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
				ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
				ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
				Thread.sleep(2000);
				fileAttachBtn.click();
				validateFileUpload(txtFilePath,subCatgry);
				fileBtnOption1.click();
				validateFileUpload(csvFilePath,subCatgry);
				fileBtnOption2.click();
				validateFileUpload(xlsxFilePath, subCatgry);
				validateContinue();
			return new AccountChangesServiceInquiresReviewPageAction();
				
				}
		
	//Terminations - Remove Spouse/Child
				public AccountChangesServiceInquiresReviewPageAction validateRemoveSpouseChildSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN, String EffDateOfChange,
						String pdfFilePath,String txtExceedSizePath, String PNGFileType) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					EnterText(effDateOfChange,EffDateOfChange, "Effective Date of Change");
					fileEmployeeRfcform.click();
					validateFileSizeErrorMsg(fileErrorMesgRfcForm,txtExceedSizePath, subCatgry);
					fileEmployeeRfcform.click();
					validateFileTypeErrorMsg(fileErrorMesgRfcForm,PNGFileType,subCatgry);
					fileEmployeeRfcform.click();
					validateFileUpload(pdfFilePath,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateRemoveSpouseChildSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN, String EffDateOfChange,
						String FilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					EnterText(effDateOfChange,EffDateOfChange, "Effective Date of Change");
					fileEmployeeRfcform.click();
					validateFileUpload(FilePath,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
		//Other Employee Changes - Class Change
				public AccountChangesServiceInquiresReviewPageAction validateClassChange(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN, String ClassType1, String CurrentClass1,String NewClass1,
			    		String ClassType2,String CurrentClass2,String NewClass2,String ClassType3,String CurrentClass3,String NewClass3,String ClassType4,String CurrentClass4,String NewClass4,String EffDateOfChange) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					scrollIntoView(effDateOfChange, driver);
					
					for(int i=1; i<=3; i++) {
						AddCoverageBtn.click();
						Thread.sleep(3000);
					}
					//First Row in Class Type table
					ComboSelectValue(classType1,ClassType1,"Class Type");
					wait.waitForElementToDisplay(driver, currentClass1);
					EnterText(currentClass1,CurrentClass1,"Current Class");
					EnterText(newClass1,NewClass1,"New Class");
					Thread.sleep(2000);
					//Second Row in Class Type table
					ComboSelectValue(classType2,ClassType2,"Class Type");
					wait.waitForElementToDisplay(driver, currentClass2);
					EnterText(currentClass2,CurrentClass2,"Current Class");
					EnterText(newClass2,NewClass2,"New Class");
					Thread.sleep(2000);
					//Third Row in Class Type table
					ComboSelectValue(classType3,ClassType3,"Class Type");
					wait.waitForElementToDisplay(driver, currentClass3);
					EnterText(currentClass3,CurrentClass3,"Current Class");
					EnterText(newClass3,NewClass3,"New Class");
					Thread.sleep(2000);
					//Fourth Row in Class Type table
					ComboSelectValue(classType4,ClassType4,"Class Type");
					wait.waitForElementToDisplay(driver, currentClass3);
					EnterText(currentClass4,CurrentClass4,"Current Class");
					EnterText(newClass4,NewClass4,"New Class");
					Thread.sleep(2000);
					EnterText(effDateOfChange,EffDateOfChange, "Effective Date of Change");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//Other Employee Changes - Salary Change (Not Bulk)
				public AccountChangesServiceInquiresReviewPageAction validateSalaryChangeNotBulk(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN,String NewSalary, String SalaryType, String NewSalaryEffectiveDate) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					EnterText(txtNewSalary,NewSalary, "New Salary");
					Thread.sleep(2000);
					ComboSelectValue(dropdownSalaryType, SalaryType,"Salary Type");
					EnterText(txtNewSalaryEffDate,NewSalaryEffectiveDate,"New Salary Effective Date");
					
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
		//Other Employee Changes - Salary Change - Bulk (full Census)
				public AccountChangesServiceInquiresReviewPageAction validateSalaryChangeBulkSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String EffDateOfChange,
						String PNGFileType,String csvFilePath,String txtFilePath,String xlsxFilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(effDateOfChange,EffDateOfChange, "Effective Date of Change");
					fileCensusForm.click();
					validateFileTypeErrorMsg(fileTypeCensusFormErr,PNGFileType,subCatgry);
					fileCensusForm.click();
					validateFileUpload(csvFilePath,subCatgry);
					fileOptionalCensusForm.click();
					validateFileTypeOptionalErrorMsg(fileOptionalCensusFormErr,txtFilePath,subCatgry);
					fileOptionalCensusForm.click();
					validateFileUpload(xlsxFilePath,subCatgry);
					validateContinue();
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateSalaryChangeBulkSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String EffDateOfChange,
						String xlsExceedFilePath,String xlsxFilePath, String csvFilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(effDateOfChange,EffDateOfChange, "Effective Date of Change");
					Thread.sleep(2000);
					fileCensusForm.click();
					validateFileSize13MBErrorMsg(fileSizeExceedCensusFormError,xlsExceedFilePath,subCatgry);
					fileCensusForm.click();
					validateFileUpload(xlsxFilePath,subCatgry);
					Thread.sleep(2000);
					fileOptionalCensusForm.click();
					validateFileSize13MBOptionalErrorMsg(fileSizeExceedOptionalCensusFormError,xlsExceedFilePath,subCatgry);
					fileOptionalCensusForm.click();
					validateFileUpload(csvFilePath,subCatgry);
					validateContinue();
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
		//Other Employee Changes - Name Change
				public AccountChangesServiceInquiresReviewPageAction validateNameChange(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeCurntFirstName,String eeCurntLastname,String eeSSN,String eeNewFirstName, String eeNewLastName) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeCurntFirstName, "EE Current First Name");
					EnterText(txtEELstname,eeCurntLastname,"EE Current Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					EnterText(txteeNewFName,eeNewFirstName, "New First Name");
					EnterText(txteeNewLName, eeNewLastName,"New Last Name");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		
		//Other Employee Changes - Address Change
				public AccountChangesServiceInquiresReviewPageAction validateAddressChange(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFirstName,String eeLastname,String eeSSN,String eeNewStreetAddress, String eeNewCity, String eeNewState, String eeNewZipCode) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFirstName, "EE First Name");
					EnterText(txtEELstname,eeLastname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					EnterText(txteeNewAddress,eeNewStreetAddress, "EE New Street Address");
					EnterText(txteeNewCity, eeNewCity,"EE New City");
					EnterText(txteeNewState, eeNewState,"EE New State");
					EnterText(txteeNewZipCode, eeNewZipCode,"EE New Zip Code");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
			//Other EE Changes - Beneficiary
				public AccountChangesServiceInquiresReviewPageAction validateBeneficiarySet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN,
						String txtExceedSizePath, String PNGFileType,String pdfFilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);	
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					scrollIntoView(fileBeneficiaryform, driver);
					fileBeneficiaryform.click();
					validateFileSizeErrorMsg(fileBeneficiaryErr,txtExceedSizePath, subCatgry);
					fileBeneficiaryform.click();
					validateFileTypeErrorMsg(fileBeneficiaryErr,PNGFileType,subCatgry);
					Thread.sleep(2000);
					fileBeneficiaryform.click();
					validateFileUpload(pdfFilePath,subCatgry);
					
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateBeneficiarySet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String eeFname,String eeLname,String eeSSN,
						String FilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtEEfname, eeFname, "EE First Name");
					EnterText(txtEELstname,eeLname,"EE Last Name");
					EnterText(txtSSN,eeSSN,"EE SSN");
					fileBeneficiaryform.click();
					validateFileUpload(FilePath,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
				//Other EE Changes - GIB
				public AccountChangesServiceInquiresReviewPageAction validateGIBSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String PNGFileType,String txtExceedSizePath,String txtFilePath,String xlsxFilePath,String pdfFilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					fileGIBMandatory.click();
					validateFileTypeErrorMsg(FileGIBMandatoryErr,PNGFileType,subCatgry);
					fileGIBMandatory.click();
					validateFileSize13MBErrorMsg(FileGIBMandatoryErr,txtExceedSizePath, subCatgry);
					fileGIBMandatory.click();
					validateFileUpload(txtFilePath,subCatgry);
					scrollIntoView(fileGIBOption, driver);
					fileGIBOption.click();
					validateFileTypeOptionalErrorMsg(FileGIBOptionalErr,xlsxFilePath,subCatgry);
					fileGIBOption.click();
					validateFileSize13MBOptionalErrorMsg(FileGIBOptionalErr,txtExceedSizePath, subCatgry);
					fileGIBOption.click();
					validateFileUpload(pdfFilePath,subCatgry);
					validateContinue();
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
				public AccountChangesServiceInquiresReviewPageAction validateGIBSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String FilePath1,String FilePath2) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					fileGIBMandatory.click();
					validateFileUpload(FilePath1,subCatgry);
					fileGIBOption.click();
					validateFileUpload(FilePath2,subCatgry);
					validateContinue();
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
		//Billing Inquiries - Submit Summary Bill(s)
				public AccountChangesServiceInquiresReviewPageAction validateSubmitSummaryBillSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String PNGFileType,String txtExceedSizePath,String txtFilePath,String xlsxFilePath,String pdfFilePath,String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					fileSSBMandatory.click();
					validateFileTypeErrorMsg(fileSSBMandatoryErr,PNGFileType,subCatgry);
					fileSSBMandatory.click();
					validateFileSizeErrorMsg(fileSSBMandatoryErr,txtExceedSizePath, subCatgry);
					fileSSBMandatory.click();
					validateFileUpload(txtFilePath,subCatgry);
					scrollIntoView(fileSSBMandatory, driver);
					fileBtnOption1.click();
					validateFileTypeOptionalErrorMsg(fileErrorMsgOption1,PNGFileType,subCatgry);
					fileBtnOption1.click();
					validateFileSizeOptionalErrorMsg(fileErrorMsgOption1,txtExceedSizePath, subCatgry);
					fileBtnOption1.click();
					validateFileUpload(xlsxFilePath,subCatgry);
					fileBtnOption2.click();
					validateFileSizeOptionalErrorMsg(fileErrorMsgOption2,txtExceedSizePath, subCatgry);
					fileBtnOption2.click();
					validateFileUpload(pdfFilePath,subCatgry);
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateSubmitSummaryBillSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String FilePath1,String FilePath2,String FilePath3,String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					fileSSBMandatory.click();
					validateFileUpload(FilePath1,subCatgry);
					scrollIntoView(fileSSBMandatory, driver);				
					fileBtnOption1.click();
					validateFileUpload(FilePath2,subCatgry);
		
					fileBtnOption2.click();
					validateFileUpload(FilePath3,subCatgry);
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				
					
	//Billing Inquiries	- Notification of Payment		
				public AccountChangesServiceInquiresReviewPageAction validateNotificationOfPaymentSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String PayAmount, String Note,
						String PNGFileType,String txtExceedSizePath,String txtFilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtPayAmount, PayAmount ,"Pay Amount");
					EnterText(txtNotes, Note ,"Notes");
					fileNOPForm.click();
					validateFileTypeErrorMsg(fileNOPErr,PNGFileType,subCatgry);
					fileNOPForm.click();
					validateFileSize10MBErrorMsg(fileNOPErr,txtExceedSizePath, subCatgry);
					fileNOPForm.click();
					validateFileUpload(txtFilePath,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
				public AccountChangesServiceInquiresReviewPageAction validateNotificationOfPaymentSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String PayAmount, String Note,
						String FilePath) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtPayAmount, PayAmount ,"Pay Amount");
					EnterText(txtNotes, Note ,"Notes");
					fileNOPForm.click();
					validateFileUpload(FilePath,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//Billing Inquiries - Where is my bill?
				public AccountChangesServiceInquiresReviewPageAction validateWhereIsMyBill(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String MonthYear, String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Please check the Self-Billing in iBill and Bill History training videos. If you are unable to locate the information you need, please indicate the month and year for your requested information.","Message");
					Thread.sleep(2000);
					EnterText(txtMonthYear, MonthYear ,"Month Year");
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
	//Billing Inquiries - Copy of Bill
				public AccountChangesServiceInquiresReviewPageAction validateCopyOfBill(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String MonthYear, String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Please check the Self-Billing in iBill and Bill History training videos. If you are unable to locate the information you need, please indicate the month and year for your requested information.","Message");
					Thread.sleep(2000);
					EnterText(txtMonthYear, MonthYear ,"Month Year");
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//Billing Inquiries - Why did my bill change?
				public AccountChangesServiceInquiresReviewPageAction validateWhyDidMyBillChange(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String MonthYear, String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Please indicate the month, year, and bill change below.","Message");
					Thread.sleep(2000);
					EnterText(txtMonthYear, MonthYear ,"Month Year");
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
	//Billing Inquiries - iBill Questions - How Do I?
				public AccountChangesServiceInquiresReviewPageAction validateHowDoI(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Please check the training videos or the User Guide. If you are unable to locate the information you need, please indicate the month and year for your requested information.","Message");
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//Billing Inquiries - Other
				public AccountChangesServiceInquiresReviewPageAction validateOther(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String Note, String FileType,String FileSize,String File) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Please check the FAQ & Resources menu item. If you are unable to locate the information you need, please provide a detailed description of your request so we may be of assistance. Thank you for choosing OneAmerica.","Message");
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					OtherBtn.click();
					validateFileTypeErrorMsg(fileOtherErr,FileType,subCatgry);
					OtherBtn.click();
					validateFileSize10MBErrorMsg(fileOtherErr,FileSize, subCatgry);
					OtherBtn.click();
					validateFileUpload(File,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
	//General Inquiries - Billing Contact Change
				public AccountChangesServiceInquiresReviewPageAction validateBillingContactChange(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry, String ContactFN, String ContactLN,String ContactEmail,String Phone,String EffDate) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(NewContactFN, ContactFN ,"New Contact First Name");
					scrollIntoView(NewContactLN, driver);
					EnterText(NewContactLN, ContactLN ,"New Contact Last Name");
					EnterText(NewContactEmail, ContactEmail ,"New Contact Email");
					EnterText(NewContactPhone,Phone ,"New Contact Phone Number");
					Thread.sleep(2000);
					scrollIntoView(EffectiveDate, driver);
					EnterText(EffectiveDate,EffDate ,"Effective Date");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
	//General Inquiries - IBill Access
				public AccountChangesServiceInquiresReviewPageAction validateIBillAccess(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String NameOfEE ,String Note, String FileType,String FileSize,String File) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtNameOfEE, NameOfEE ,"Name of Employee That Needs Access");
					EnterText(txtNotes, Note ,"Notes");
					Thread.sleep(2000);
					assertText(txtBillMessage,"Administrative Options Election Form for Group Insurance Policyholders (G-17577) can be found in the Forms section of eBen under the 'Policy/Employee Admin' tab.","Message");
					fileIBillBtn.click();
					validateFileTypeErrorMsg(fileIBillAccessErr,FileType,subCatgry);
					fileIBillBtn.click();
					validateFileSizeErrorMsg(fileIBillAccessErr,FileSize, subCatgry);
					fileIBillBtn.click();
					validateFileUpload(File,subCatgry);
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
				}				
				
		//General Inquiries - Multiple Changes
				public AccountChangesServiceInquiresReviewPageAction validateMultipleChanges(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String FileType,String FileSizePath,String FilePath,String FilePath2,String FilePath3) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					fileMultipleChangeAttach1.click();
					validateFileTypeErrorMsg(fileMultipleChangesErr1,FileType,subCatgry);
					fileMultipleChangeAttach1.click();
					validateFileSizeErrorMsg(fileMultipleChangesErr1,FileSizePath, subCatgry);
					fileMultipleChangeAttach1.click();
					validateFileUpload(FilePath,subCatgry);
					scrollIntoView(fileMultipleChangeAttach1, driver);
					fileMultipleChangeAttach2.click();
					validateFileTypeErrorMsg(fileMultipleChangesErr2,FileType,subCatgry);
					fileMultipleChangeAttach2.click();
					validateFileSizeErrorMsg(fileMultipleChangesErr2,FileSizePath, subCatgry);
					fileMultipleChangeAttach2.click();
					validateFileUpload(FilePath2,subCatgry);
					fileBtnOption1.click();
					validateFileSizeOptionalErrorMsg(fileErrorMsgOption1,FileSizePath, subCatgry);
					fileBtnOption1.click();
					validateFileUpload(FilePath3,subCatgry);
					
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//General Inquiries - Status
				public AccountChangesServiceInquiresReviewPageAction validateStatus(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
				}	
		//General Inquiries - Cert/Policy Copies
				public AccountChangesServiceInquiresReviewPageAction validateCertPolicyCopies(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String Note) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					EnterText(txtNotes, Note ,"Notes");
					scrollIntoView(txtNotes, driver);
					assertText(InfoMsg,"Please check the  Benefit Pages video or the User Guide. If you are unable to locate the information you need, please provide a detailed description of your request so we may be of assistance.","Message");
					
					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
				}
			
	//General Inquiries - Agent/Broker of Record Change
				public AccountChangesServiceInquiresReviewPageAction validateAgentBrokerOfRecordChangeSet1(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String FileType,String FileSizePath,String FilePath1,String FilePath2) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					assertText(InfoMsg,"Complete the Agent of Record Change Request for Group Insurance Policyholders (G20120) and submit it as an attachment on company letterhead or email. Thank you for choosing OneAmerica.","Message");
					Thread.sleep(2000);
					fileABRecordChangeAttach1.click();
					validateFileTypeErrorMsg(fileABRecordChangeErr,FileType,subCatgry);
					fileABRecordChangeAttach1.click();
					validateFileSize13MBErrorMsg(fileABRecordChangeErr,FileSizePath, subCatgry);
					fileABRecordChangeAttach1.click();
					validateFileUpload(FilePath1,subCatgry);
					scrollIntoView(fileABRecordChangeAttach2, driver);
					fileABRecordChangeAttach2.click();
					validateFileTypeOptionalErrorMsg(fileABRecordChangeOptionalErr,FileType,subCatgry);
					fileABRecordChangeAttach2.click();
					validateFileSize13MBOptionalErrorMsg(fileABRecordChangeOptionalErr,FileSizePath, subCatgry);
					fileABRecordChangeAttach2.click();
					validateFileUpload(FilePath2,subCatgry);

					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}	
				public AccountChangesServiceInquiresReviewPageAction validateAgentBrokerOfRecordChangeSet2(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,
						String FilePath1,String FilePath2) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					assertText(InfoMsg,"Complete the Agent of Record Change Request for Group Insurance Policyholders (G20120) and submit it as an attachment on company letterhead or email. Thank you for choosing OneAmerica.","Message");
					Thread.sleep(2000);
					fileABRecordChangeAttach1.click();
					validateFileUpload(FilePath1,subCatgry);
					scrollIntoView(fileABRecordChangeAttach2, driver);
					fileABRecordChangeAttach2.click();
					validateFileUpload(FilePath2,subCatgry);

					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//General Inquiries - Other
				public AccountChangesServiceInquiresReviewPageAction validateGeneralInquiriesOther(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry,String Note,
						String FileType,String FileSizePath,String FilePath1,String FilePath2) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					assertText(InfoMsg,"Please check the FAQ & Resources menu item. If you are unable to locate the information you need, please provide a detailed description of your request so we may be of assistance. Thank you for choosing OneAmerica.","Message");
					EnterText(txtNotes, Note ,"Notes");
					Thread.sleep(2000);
					scrollIntoView(fileBtnOption1, driver);
					fileBtnOption1.click();
					validateFileTypeOptionalErrorMsg(fileErrorMsgOption1,FileType,subCatgry);
					fileBtnOption1.click();
					validateFileSizeOptionalErrorMsg(fileErrorMsgOption1,FileSizePath, subCatgry);
					fileBtnOption1.click();
					validateFileUpload(FilePath1,subCatgry);
					fileBtnOption2.click();
					validateFileTypeOptionalErrorMsg(fileErrorMsgOption2,FileType,subCatgry);
					fileBtnOption2.click();
					validateFileSizeOptionalErrorMsg(fileErrorMsgOption2,FileSizePath, subCatgry);
					fileBtnOption2.click();
					validateFileUpload(FilePath2,subCatgry);

					validateContinue();
					
					return new AccountChangesServiceInquiresReviewPageAction();
					
				}
		//Group Disability and Life Claims - Group Disability Claim
				public AccountChangesServiceInquiresReviewPageAction validateGroupDisabilityClaim(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(InfoMsg,"For assistance with Disability claims, please contact oneamerica.claims@customdisability.com.","Message");
					
					return new AccountChangesServiceInquiresReviewPageAction();
				}
		//Group Disability and Life Claims - Group Disability Claim
				public AccountChangesServiceInquiresReviewPageAction validateLifeClaim(String policyHolderName,String policyHolderNumber,
						String requesterType,String firstName,String lastName,String email,String phone,String requestTyp,String subCatgry) throws Exception {
					
					validatePolicyAndRequesterInfo(policyHolderName,policyHolderNumber,requesterType,firstName,lastName,email,phone);
					
					//Type Of Request
					scrollIntoView(txtPhone, driver);
					takeScreenshot("Account Changes_Type of Request Up");
					assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
					ComboSelectValue(dropdownRequestTyp,requestTyp,"Request Type");
					ComboSelectValue(dropdownSubCategory, subCatgry,"Sub Category");
					Thread.sleep(2000);
					assertText(InfoMsg,"For assistance with Life claims, please contact lifeclaims.employeebenefits@oneamerica.com.","Message");
					return new AccountChangesServiceInquiresReviewPageAction();
				}
				
		  public void  validateFileUpload(String filePath,String subCatgry ) throws Exception {
			  Robot robot=new Robot();
			  StringSelection stringSelection=new StringSelection(filePath);
			  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection,null);
			  robot.setAutoDelay(1000);
			  robot.keyPress(KeyEvent.VK_CONTROL);
			  robot.keyPress(KeyEvent.VK_V);
			  robot.keyRelease(KeyEvent.VK_CONTROL);
			  robot.keyRelease(KeyEvent.VK_V);
			  robot.keyPress(KeyEvent.VK_ENTER);
			  robot.keyRelease(KeyEvent.VK_ENTER);
			  takeScreenshot("FileUpload Screen"+"---"+subCatgry);
			  extentTest.log(LogStatus.INFO,"Add file button is selected and File is Uploaded");
		    }
		
	   	  public void validateOption1ErrorMsg(String txtSizeExceed,String subCatgry) throws Exception {
  	    	 validateFileUpload(txtSizeExceed,subCatgry);
  	
  	    	  takeScreenshot("File upload File size exceeds 8MB size limit"+"----"+subCatgry);
  	     	  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 8mb is passed in Optional field");
  	          assertText(fileErrorMsgOption1,errorMessage,"File exceeding 8mb");
  	    }
	   	  public void validateEnrollFormErrorMsg(String txtSizeExceed,String subCatgry) throws Exception {
     	      validateFileUpload(txtSizeExceed,subCatgry);
 
     	   	  takeScreenshot("File upload File size exceeds 8MB size limit"+"----"+subCatgry);
     		  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 8mb is passed in mandatory field");
     		  assertText(fileErrorMsgEnrollForm,errorMessage,"File exceeding 8mb");
	   	  }
	 
	 	 
	 	//Added by Saivenkat Korivi on 3/18/2022  
	 	public void validateFileTypeErrorMsg(WebElement element,String FileType,String subCatgry) throws Exception {
	 		Thread.sleep(3000);
	 		validateFileUpload(FileType,subCatgry);
	   	   	  takeScreenshot("File upload Unsupported File Type"+"----"+subCatgry);
	   		  extentTest.log(LogStatus.INFO,"Add file button is selected and Unsupported file type is passed in mandatory field");
	   		  assertText(element,errorMessageFileType,"The file is not an accepted file type");
	   	    }
	 	public void validateFileTypeOptionalErrorMsg(WebElement element,String FileType,String subCatgry) throws Exception {
	 		Thread.sleep(3000);
	 		validateFileUpload(FileType,subCatgry);
	   	   	  takeScreenshot("File upload Unsupported File Type"+"----"+subCatgry);
	   		  extentTest.log(LogStatus.INFO,"Add file button is selected and Unsupported file type is passed in Optional field");
	   		  assertText(element,errorMessageFileType,"The file is not an accepted file type");
	   	    }
	 	
	 	public void validateFileSizeErrorMsg(WebElement element,String FileSize,String subCatgry) throws Exception {
 	    	 validateFileUpload(FileSize,subCatgry);
 	
 	    	  takeScreenshot("File upload File size exceeds 8MB size limit"+"----"+subCatgry);
 	     	  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 8mb is passed in mandatory field");
 	          assertText(element,errorMessage,"File exceeding 8mb");
 	          
 	    }
	 	public void validateFileSizeOptionalErrorMsg(WebElement element,String FileSize,String subCatgry) throws Exception {
	    	 validateFileUpload(FileSize,subCatgry);
	
	    	  takeScreenshot("File upload File size exceeds 8MB size limit"+"----"+subCatgry);
	     	  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 8mb is passed in Optional field");
	          assertText(element,errorMessage,"File exceeding 8mb");
	          
	    }
	 	public void validateFileSize13MBErrorMsg(WebElement element,String FileSize,String subCatgry) throws Exception {
	    	 validateFileUpload(FileSize,subCatgry);
	
	    	  takeScreenshot("File upload File size exceeds 13MB size limit"+"----"+subCatgry);
	     	  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 13mb is passed in mandatory field");
	          assertText(element,errorMessage13mb,"File exceeding 13mb");
	    }
	 	public void validateFileSize10MBErrorMsg(WebElement element,String FileSize,String subCatgry) throws Exception {
   	      validateFileUpload(FileSize,subCatgry);

   	   	  takeScreenshot("File upload File size exceeds 10MB size limit"+"----"+subCatgry);
   		  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 10mb is passed in mandatory field");
   		  assertText(element,errorMessage10mb,"File size exceeds 10MB size limit");
   	    }
	 	public void validateFileSize13MBOptionalErrorMsg(WebElement element,String FileSize,String subCatgry) throws Exception {
	   	      validateFileUpload(FileSize,subCatgry);

	   	   	  takeScreenshot("File upload File size exceeds 13MB size limit"+"----"+subCatgry);
	   		  extentTest.log(LogStatus.INFO,"Add file button is selected and file having size exceeding 13mb is passed in Optional field");
	   		  assertText(element,errorMessage13mb,"File size exceeds 13MB size limit");
	   	    }
	 	//Added by Saivenkat Korivi on 03/21/2022
	 	//To validate Policy info and Requester Info Section
	 	public void validatePolicyAndRequesterInfo(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,String email,String phone) throws Exception {
	    	 
	 		//Policy Information Section
			assertText(headerPolicyInformation,"Policy Information","Policy Information header");
			EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
			EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
			takeScreenshot("Account Changes_PolicyInformationSection");
			
			//RequesterInformation Section
			assertText(headerRequesterInfo,"Requester Information","Requester Information header");
			ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
			EnterText(txtFirstName, firstName, "First Name");
			EnterText(txtLastName, lastName,"Last Name");
			EnterText(txtEmail,email,"Email");
			EnterText(txtPhone, phone,"Phone Number");
			takeScreenshot("Account Changes_RequesterInformation");
	 		
	    }
	 	
	 	
	 	
		public void selectCoverage(String life,String add,String std,String ltd) throws Exception {
			Thread.sleep(3000);
			
			if(life.equalsIgnoreCase("Yes")) {
				ClickElement(chckboxBaseLife," Base Life");
			}
			if(add.equalsIgnoreCase("Yes")) {
				ClickElement(chckboxADD,"ADD");
			}
			if(std.equalsIgnoreCase("Yes")) {
				ClickElement(chckboxSTD,"DLF");
			}
			if(ltd.equalsIgnoreCase("Yes")) {
				ClickElement(chckboxLTD,"DLF");
			}
			takeScreenshot("Account Changes_Select Coverage");
			}
			
		  //method for selecting Continue button
		   public void validateContinue() throws Exception {
			scrollPageDown(driver);
			ClickElement(btnContinue,"Continue");
			Thread.sleep(3000);
		   }	
	
	}

