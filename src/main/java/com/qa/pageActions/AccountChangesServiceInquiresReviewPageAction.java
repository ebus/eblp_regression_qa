package com.qa.pageActions;

import org.openqa.selenium.By;

import com.qa.page.AccountChangesServiceInquiresReviewPage;
import com.qa.page.PolicyBillingDetailsReviewPage;
import com.qa.util.GenericFunction;
import com.relevantcodes.extentreports.LogStatus;


public class AccountChangesServiceInquiresReviewPageAction extends AccountChangesServiceInquiresReviewPage {
	public AccountChangesServiceInquiresReviewPageAction() {
		
		
		super();
	
	}
	/* *****************************************************************************
	  * Test Name : PolicyBillingDetailsReviewPageAction 
	  * Purpose : This class contains the action of  the PolicyBillingDetailsReview page
	  * History : Created by Anjali Johny on 09/27/2021 
	  **************************************************************************************/
public ConfirmationPageAction validateReviewPage() throws Exception {
	
	Thread.sleep(4000);
    scrollPageUp(driver);
    takeScreenshot("Account Changes_Review_Screen1");
    assertElementDisplayed(headerPolicyInfo,"Policy Information header");
    scrollIntoView(headerPolicyInfo, driver);
    takeScreenshot("Account Changes_Review_Screen2");
    assertElementDisplayed(headerRequesterInfo,"Requester Information header");
    scrollIntoView(headerRequesterInfo, driver);
    takeScreenshot("Account Changes_Review_Screen3");
    assertElementDisplayed(headerTypeOfRqst,"Type of Request header");
	assertElementDisplayed(btnEdit,"Edit button");
	assertElementDisplayed(btnSubmit,"Submit button");
	scrollIntoView(btnSubmit, driver);
	takeScreenshot("Account Changes_Review_Screen4");
	extentTest.log(LogStatus.INFO,"Account Changes Service Inquires - Review page is displayed");
    ClickElement(btnSubmit,"Submit Button");
	//return new ConfirmationPageAction();
	return new ConfirmationPageAction();
			
			
}	


	}