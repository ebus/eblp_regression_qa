package com.qa.pageActions;
import com.qa.page.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageAction extends LoginPage {
	public LoginPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : LoginPageAction 
	  * Purpose : This class contains the action of  the login page
	  * History : Created by Anjali Johny on 09/22/2021 
	  **************************************************************************************/
	public HomePageAction userLogin(String userId, String passWord) throws InterruptedException {
		extentTest.log(LogStatus.INFO, "------- LoginPage -------");
		ClickElement(btn_Login, "Login");
		EnterText(txtUserName, userId, "UserID");		
		EnterPassword(txtPassword, passWord, "PassWord");
		takeScreenshot("LoginPage");
		ClickElement(btn_OALogin, "Login");
	return new HomePageAction();
	}
	}