package com.qa.pageActions;


import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.AWTException;
import java.awt.RenderingHints.Key;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.qa.page.HomePage;
import com.qa.page.PolicyBillingDetailPage;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;


public class PolicyBillingDetailPageAction extends PolicyBillingDetailPage {
	public PolicyBillingDetailPageAction() {
		super();
	}
	
	String errorMessage="File size exceeds 5MB size limit";
 
	/* *****************************************************************************
	  * Test Name : PolicyBillingPageAction 
	  * Purpose : This class contains the action of  the PolicyBilling page
	  * History : Created by Anjali Johny on 10/26/2021 
	  **************************************************************************************/
	
	public void validateHomePage(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
			     String email,String phone,String reqEffDate,String activeClaim,String life,String add,String dlf,String dad,String vtl,
			     String vta,String dvt,String dva,String std,String ltd,String wds,String wdl,String ols,String classno,String changeRqst) throws Exception  {
		Thread.sleep(1000);
		
		Robot robot = new Robot();
		//Policy Information Section
		takeScreenshot("Policy Billing Detail_PolicyInformationSection");
		assertText(headerPolicyInformation,"Policy Information","Policy Information header");
		EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
		EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
		
		//RequesterInformation Section
		assertText(headerRequesterInfo,"Requester Information","Requester Information header");
		ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
		EnterText(txtFirstName, firstName, "First Name");
		EnterText(txtLastName, lastName,"Last Name");
		EnterText(txtEmail,email,"Email");
		EnterText(txtPhone, phone,"Phone Number");
		takeScreenshot("Policy Billing Detail_RequesterInformation");
		
		//Type Of Request
		scrollIntoView(txtPhone, driver);
		takeScreenshot("Policy Billing Detail_Type of Request Up");
		assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
	    EnterText(txtDate,reqEffDate,"Requested Effective Date");
		robot.keyPress(KeyEvent.VK_TAB);
	    ComboSelectValue(chckboxActiveClaim,activeClaim,"Active Claim");
	    selectCoverage(life,add,dlf,dad,vtl,vta,dvt,dva,std,ltd,wds,wdl,ols);
	    EnterText(txtClassNumber, classno,"Class Number");
	    ComboSelectValue(dropdownChangeRqsted, changeRqst,"Change Requested");
	    Thread.sleep(2000);
		takeScreenshot("Policy Billing Detail_Type of Request Down");
	}
	
	
	//**** method used to click on Select ALL checkbox ***//
	public void validateHomePagewithSelectALLCov(String policyHolderName,String policyHolderNumber,String requesterType,String firstName,String lastName,
		     String email,String phone,String reqEffDate,String activeClaim,String classno,String changeRqst) throws Exception  {
	Thread.sleep(1000);
	
	Robot robot = new Robot();
	//Policy Information Section
	takeScreenshot("Policy Billing Detail_PolicyInformationSection");
	assertText(headerPolicyInformation,"Policy Information","Policy Information header");
	EnterText(txtPolicyHolderName,policyHolderName,"policyHolderName Text");
	EnterText(txtPolicyHolderNumber,policyHolderNumber,"policyHolderNumber text");
	
	//RequesterInformation Section
	assertText(headerRequesterInfo,"Requester Information","Requester Information header");
	ComboSelectValue(dropdownRequesterType,requesterType,"Requester Type");
	EnterText(txtFirstName, firstName, "First Name");
	EnterText(txtLastName, lastName,"Last Name");
	EnterText(txtEmail,email,"Email");
	EnterText(txtPhone, phone,"Phone Number");
	takeScreenshot("Policy Billing Detail_RequesterInformation");
	
	//Type Of Request
	scrollIntoView(txtPhone, driver);
	takeScreenshot("Policy Billing Detail_Type of Request Up");
	assertText(headerTypeOfRequest,"Type of Request","Type of Request header");
	EnterText(txtDate,reqEffDate,"Requested Effective Date");
	robot.keyPress(KeyEvent.VK_TAB);
	ComboSelectValue(chckboxActiveClaim,activeClaim,"Active Claim");
   	ClickElement(chckboxSelectAll, "Select ALL checkbox");
    EnterText(txtClassNumber, classno,"Class Number");
    ComboSelectValue(dropdownChangeRqsted, changeRqst,"Change Requested");
    Thread.sleep(2000);
	takeScreenshot("Policy Billing Detail_Type of Request Down");
	}
	

	
	//********** File Upload method used for Change Requested_2004-55 testcase using ROBOT*********//
     	public PolicyBillingDetailsReviewPageAction validateFileUploadSet1(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String changeRqst) throws Exception {
     		fileUploadPolicy.click();
     		validatePolicyHolderErrorMsg(docSizeExceed,changeRqst);
     		fileUploadPolicy.click();
    		validateFileUpload(docurl,changeRqst);
    		fileUploadOption1.click();
    		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
    		fileUploadOption1.click();
    		validateFileUpload(txturl,changeRqst);
    		fileUploadOption2.click();
    		validateFileUpload(pdfurl,changeRqst);
    		fileUploadOption3.click();
    		validateFileUpload(xlsxurl,changeRqst);
    		fileUploadOption4.click();
    		validateFileUpload(csvurl,changeRqst);
    		validateContinue();
		return new PolicyBillingDetailsReviewPageAction();

		}
     	  public PolicyBillingDetailsReviewPageAction validateFileUploadSet2(String pdfurl,String docurl,String changeRqst) throws Exception {
     			fileUploadPolicy.click();
        		validateFileUpload(pdfurl,changeRqst);
        		fileUploadOption1.click();
        		validateFileUpload(docurl,changeRqst);
     			validateContinue();
     			Thread.sleep(3000);
     			return new PolicyBillingDetailsReviewPageAction() ;
     		  }
     	 public PolicyBillingDetailsReviewPageAction validateFileUploadSet3(String txturl,String changeRqst) throws Exception {
     		fileUploadPolicy.click();
    		validateFileUpload(txturl,changeRqst);
     		validateContinue();
     		Thread.sleep(3000);
     		return new PolicyBillingDetailsReviewPageAction() ;
     	  } 
     	 
      	//**********Admin File Upload *********//
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet4(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
      		validateAdminOptionsError(docSizeExceed,changeRqst);
       		fileAdminOptionsElectionForm.click();
      		validateFileUpload(docurl,changeRqst);
      		fileUploadOption1.click();
      		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
      		fileUploadOption1.click();
      		validateFileUpload(txturl,changeRqst);
      		fileUploadOption2.click();
      		validateFileUpload(pdfurl,changeRqst);
      		fileUploadOption3.click();
      		validateFileUpload(xlsxurl,changeRqst);
      		fileUploadOption4.click();
      		validateFileUpload(csvurl,changeRqst);
      		validateContinue();
  		return new PolicyBillingDetailsReviewPageAction();
  		} 
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet5(String pdfurl,String docurl,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
     		validateFileUpload(pdfurl,changeRqst);
     		fileUploadOption1.click();
     		validateFileUpload(docurl,changeRqst);
  			validateContinue();
  			Thread.sleep(3000);
  		return new PolicyBillingDetailsReviewPageAction() ;
  		  }
      	
      	
      	
      	
      	 
      	//**********Admin -Census File Upload *********//
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet8(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String xlsxSizeExceed,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
      		validateAdminOptionsError(docSizeExceed,changeRqst);
       		fileAdminOptionsElectionForm.click();
      		validateFileUpload(docurl,changeRqst);
      		fileCensus.click();
 			validateCensusError(xlsxSizeExceed,changeRqst);
 			fileCensus.click();
 			validateFileUpload(xlsxurl,changeRqst);
      		fileUploadOption1.click();
      		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
      		fileUploadOption1.click();
      		validateFileUpload(txturl,changeRqst);
      		fileUploadOption2.click();
      		validateFileUpload(pdfurl,changeRqst);
      		fileUploadOption3.click();
      		validateFileUpload(xlsxurl,changeRqst);
      		
      		validateContinue();
  		return new PolicyBillingDetailsReviewPageAction();
  		} 
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet9(String pdfurl,String csvurl,String docurl,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
     		validateFileUpload(pdfurl,changeRqst);
     		fileCensus.click();
 			validateFileUpload(csvurl,changeRqst);
 			fileUploadOption1.click();
      		validateFileUpload(csvurl,changeRqst);
     		fileUploadOption2.click();
     		validateFileUpload(docurl,changeRqst);
  			validateContinue();
  			Thread.sleep(3000);
  		return new PolicyBillingDetailsReviewPageAction() ;
  		  }
   	 
      	//**********Admin -PolicyHolder -Census File Upload *********//
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet10(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String xlsxSizeExceed,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
      		validateAdminOptionsError(docSizeExceed,changeRqst);
       		fileAdminOptionsElectionForm.click();
      		validateFileUpload(docurl,changeRqst);
     		fileUploadPolicy.click();
     		validatePolicyHolderErrorMsg(docSizeExceed,changeRqst);
     		fileUploadPolicy.click();
    		validateFileUpload(docurl,changeRqst);
      		fileCensus.click();
 			validateCensusError(xlsxSizeExceed,changeRqst);
 			fileCensus.click();
 			validateFileUpload(xlsxurl,changeRqst);
      		fileUploadOption1.click();
      		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
      		fileUploadOption1.click();
      		validateFileUpload(txturl,changeRqst);
      		fileUploadOption2.click();
      		validateFileUpload(pdfurl,changeRqst);      		
      		validateContinue();
  		return new PolicyBillingDetailsReviewPageAction();
  		} 
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet11(String pdfurl,String csvurl,String docurl,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
     		validateFileUpload(pdfurl,changeRqst);
     		fileUploadPolicy.click();
    		validateFileUpload(pdfurl,changeRqst);
     		fileCensus.click();
 			validateFileUpload(csvurl,changeRqst);
 			fileUploadOption1.click();
      		validateFileUpload(csvurl,changeRqst);
     		fileUploadOption2.click();
     		validateFileUpload(docurl,changeRqst);
  			validateContinue();
  			Thread.sleep(3000);
  		return new PolicyBillingDetailsReviewPageAction() ;
  		  }
    	public PolicyBillingDetailsReviewPageAction validateFileUploadSet12(String pdfurl,String csvurl,String txturl,String xlsxurl,String changeRqst) throws Exception {
      		fileAdminOptionsElectionForm.click();
     		validateFileUpload(pdfurl,changeRqst);
     		fileUploadPolicy.click();
    		validateFileUpload(txturl,changeRqst);
     		fileCensus.click();
 			validateFileUpload(csvurl,changeRqst);
 			fileUploadOption1.click();
      		validateFileUpload(xlsxurl,changeRqst);
  			validateContinue();
  			Thread.sleep(3000);
  		return new PolicyBillingDetailsReviewPageAction() ;
  		  }
      	//**************************EOP File Upload*******************************//
      	public PolicyBillingDetailsReviewPageAction validateFileUploadSet6(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String changeRqst) throws Exception {
      		fileEOP.click();
      		validateEOPError(docSizeExceed,changeRqst);
     		fileEOP.click();
    		validateFileUpload(docurl,changeRqst);
    		fileUploadOption1.click();
    		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
    		fileUploadOption1.click();
    		validateFileUpload(txturl,changeRqst);
    		fileUploadOption2.click();
    		validateFileUpload(pdfurl,changeRqst);
    		fileUploadOption3.click();
    		validateFileUpload(xlsxurl,changeRqst);
    		fileUploadOption4.click();
    		validateFileUpload(csvurl,changeRqst);
    		validateContinue();
		return new PolicyBillingDetailsReviewPageAction();

		}
     	  public PolicyBillingDetailsReviewPageAction validateFileUploadSet7(String pdfurl,String docurl,String changeRqst) throws Exception {
     		    fileEOP.click();
        		validateFileUpload(pdfurl,changeRqst);
        		fileUploadOption1.click();
        		validateFileUpload(docurl,changeRqst);
     			validateContinue();
     			Thread.sleep(3000);
     			return new PolicyBillingDetailsReviewPageAction() ;
     		  }
     	 public PolicyBillingDetailsReviewPageAction validateAddDepSet1(String changeRqst,String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String xlsxSizeExceed) throws Exception {
 			if (changeRqst.equalsIgnoreCase("Add Dependent Option")){
 				fileUploadPolicy.click();
 				validatePolicyHolderErrorMsg(docSizeExceed, changeRqst);
 				fileUploadPolicy.click();
 				validateFileUpload(docurl,changeRqst);
 			}
 			else if(changeRqst.equalsIgnoreCase("Add/Delete Class")) {
 				fileProposal.click();
 				validatePrposalError(docSizeExceed, changeRqst);
 				fileProposal.click();
 				validateFileUpload(docurl, changeRqst);
 				
 			}
 			fileCensus.click();
 			validateCensusError(xlsxSizeExceed,changeRqst);
 			fileCensus.click();
 			validateFileUpload(xlsxurl,changeRqst);
 			fileUploadOption1.click();
 			validateOption1ErrorMsg(txtSizeExceed,changeRqst);
 			fileUploadOption1.click();
 			validateFileUpload(txturl, changeRqst);
 			fileUploadOption2.click();
 			validateFileUpload(pdfurl, changeRqst);
 			fileUploadOption3.click();
 			validateFileUpload(xlsxurl, changeRqst);		
 			validateContinue();
 			return new PolicyBillingDetailsReviewPageAction();
 			}
 	  public PolicyBillingDetailsReviewPageAction validateAddDepSet2(String changeRqst,String pdfurl,String docurl,String xlsxurl,String csvurl,String xlsxSizeExceed) throws Exception {
 		   if (changeRqst.equalsIgnoreCase("Add Dependent Option")){
 				fileUploadPolicy.click();
 			   validateFileUpload(pdfurl,changeRqst);
 		   }
 		   else if(changeRqst.equalsIgnoreCase("Add/Delete Class")) {
 			   fileProposal.click();
 			validateFileUpload(pdfurl, changeRqst);
 		   }
 		   fileCensus.click();
 		   validateFileUpload(csvurl, changeRqst);
 		   fileUploadOption1.click();
 		   validateFileUpload(docurl, changeRqst);
 		   fileUploadOption2.click();
 		   validateFileUpload(csvurl,changeRqst);

 		validateContinue();
 		Thread.sleep(3000);
 		return new PolicyBillingDetailsReviewPageAction() ;
 	  }
 	   
 	   public PolicyBillingDetailsReviewPageAction validateAddDepSet3(String changeRqst,String txturl,String xlsxurl,String docurl,String xlsxSizeExceed) throws Exception {
 		   if (changeRqst.equalsIgnoreCase("Add Dependent Option")){
 			   fileUploadPolicy.click();
 			   validateFileUpload(txturl, changeRqst);
 		       
 		    }
 		   else if(changeRqst.equalsIgnoreCase("Add/Delete Class")) {
 			  fileProposal.click();
 			  validateFileUpload(docurl, changeRqst);
 		   }
 		   fileCensus.click();
 		   validateFileUpload(xlsxurl, changeRqst);
 		
 			validateContinue();
 			Thread.sleep(3000);
 			return new PolicyBillingDetailsReviewPageAction() ;
 	   }
 	//********** File Upload method used for Change Requested_Multiple Changes Requested testcase using ROBOT*********//
    	public PolicyBillingDetailsReviewPageAction validateFileUploadPolicyForm2Set1(String docurl,String txturl,String pdfurl,String xlsxurl,String docSizeExceed,String txtSizeExceed,String changeRqst) throws Exception {
    	fileUploadPolicy.click();
    	validatePolicyHolderErrorMsg(docSizeExceed,changeRqst);
    	fileUploadPolicy.click();
   		validateFileUpload(docurl,changeRqst);
   		fileUploadPolicy2.click();
		validatePolicyHolder2ErrorMsg(docSizeExceed,changeRqst);
		fileUploadPolicy2.click();
		validateFileUpload(docurl,changeRqst);
   		fileUploadOption1.click();
   		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
   		fileUploadOption1.click();
   		validateFileUpload(txturl,changeRqst);
   		fileUploadOption2.click();
   		validateFileUpload(pdfurl,changeRqst);
   		fileUploadOption3.click();
   		validateFileUpload(xlsxurl,changeRqst);
   		validateContinue();
		return new PolicyBillingDetailsReviewPageAction();

		}
    	  public PolicyBillingDetailsReviewPageAction validateFileUploadPolicyForm2Set2(String pdfurl,String docurl,String csvurl,String changeRqst) throws Exception {
    		   fileUploadPolicy.click();
       		   validateFileUpload(pdfurl,changeRqst);
       	       fileUploadPolicy2.click();
       		   validateFileUpload(pdfurl,changeRqst);
       		   fileUploadOption1.click();
       		   validateFileUpload(docurl,changeRqst);
       		   fileUploadOption2.click();
     		   validateFileUpload(csvurl,changeRqst);
    		   validateContinue();
    		   Thread.sleep(3000);
    		   return new PolicyBillingDetailsReviewPageAction() ;
    		  }
    	 public PolicyBillingDetailsReviewPageAction validateFileUploadPolicyForm2Set3(String txturl,String changeRqst) throws Exception {
    		fileUploadPolicy.click();
   		    validateFileUpload(txturl,changeRqst);
   		    fileUploadPolicy2.click();
   		    validateFileUpload(txturl,changeRqst);
    		validateContinue();
    		Thread.sleep(3000);
    		return new PolicyBillingDetailsReviewPageAction() ;
    	  } 
    	//********** File Upload method used for Change Requested_Name/Address Change testcase using ROBOT*********//
      	public PolicyBillingDetailsReviewPageAction validateFileUploadChangeRequestFormSet1(String docurl,String txturl,String pdfurl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed,String changeRqst) throws Exception {
      		fileUploadChangeRequestForm.click();
      		validateChangeRequestedFormErrorMsg(docSizeExceed,changeRqst);
      		fileUploadChangeRequestForm.click();
     		validateFileUpload(docurl,changeRqst);
     		fileUploadOption1.click();
     		validateOption1ErrorMsg(txtSizeExceed,changeRqst);
     		fileUploadOption1.click();
     		validateFileUpload(txturl,changeRqst);
     		fileUploadOption2.click();
     		validateFileUpload(pdfurl,changeRqst);
     		fileUploadOption3.click();
     		validateFileUpload(xlsxurl,changeRqst);
     		fileUploadOption4.click();
     		validateFileUpload(csvurl,changeRqst);
     		validateContinue();
 		return new PolicyBillingDetailsReviewPageAction();

 		}
      	  public PolicyBillingDetailsReviewPageAction validateFileUploadChangeRequestFormSet2(String pdfurl,String docurl,String changeRqst) throws Exception {
      		    fileUploadChangeRequestForm.click();
         		validateFileUpload(pdfurl,changeRqst);
         		fileUploadOption1.click();
         		validateFileUpload(docurl,changeRqst);
      			validateContinue();
      			Thread.sleep(3000);
      			return new PolicyBillingDetailsReviewPageAction() ;
      		  }
      	 
      	//********** Error Message methods using ROBOT*********//
     	  public void validateOption1ErrorMsg(String txtSizeExceed,String ChangeRqst) throws Exception {
     	    	 validateFileUpload(txtSizeExceed,ChangeRqst);
     	    	 scrollIntoView(txtComments, driver);
     	    	  takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+ChangeRqst);
     	     	  extentTest.log(LogStatus.INFO,"Add file button is selected and txt file having size exceeding 5mb is passed in Optional field");
     	          assertText(fileOptionError,errorMessage,"File exceeding 5mb");
     	    }
     	  public void validatePolicyHolderErrorMsg(String docSizeExceed,String changeRqst) throws Exception {
     	      validateFileUpload(docSizeExceed,changeRqst);
     	      scrollIntoView(txtComments, driver);
     	   	  takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
     		  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
     		  assertText(filePolicyError,errorMessage,"File exceeding 5mb");
     	    }
     	  public void validatePolicyHolder2ErrorMsg(String docSizeExceed,String changeRqst) throws Exception {
     	      validateFileUpload(docSizeExceed,changeRqst);
     	      scrollIntoView(txtComments, driver);
     	   	  takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
     		  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
     		  assertText(filePolicyError2,errorMessage,"File exceeding 5mb");
     	    }
     	 public void validateAdminOptionsError(String docSizeExceed,String changeRqst) throws Exception {
     		 validateFileUpload(docSizeExceed,changeRqst);
         	 scrollIntoView(txtComments, driver);
   	   	     takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
         	 takeScreenshot("Document file upload File Exceeding 5mb size");
         	 extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
         	 assertText(fileAdminError,errorMessage,"File exceeding 5mb");
         } 
     	  public void validateChangeRequestedFormErrorMsg(String docSizeExceed,String changeRqst) throws Exception {
     	      validateFileUpload(docSizeExceed,changeRqst);
     	      scrollIntoView(txtComments, driver);
     	   	  takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
     		  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
     		  assertText(fileChangeRequesrForm_Error,errorMessage,"File exceeding 5mb");
     	    }
     	public void validateEOPError(String docSizeExceed,String changeRqst) throws Exception {
   		  validateFileUpload(docSizeExceed,changeRqst);
       	  scrollIntoView(txtComments, driver);
 	   	  takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
       	  takeScreenshot("Document file upload File Exceeding 5mb size");
       	  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
       	  assertText(fileEOP_error,errorMessage,"File exceeding 5mb");
       } 
     	public void validateCensusError(String xlsxExceedSizePath,String changeRqst) throws Exception {
     		  validateFileUpload(xlsxExceedSizePath,changeRqst);
         	  scrollIntoView(txtComments, driver);
   	   	      takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
         	  takeScreenshot("Document file upload File Exceeding 5mb size");
         	  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
         	  assertText(fileCensusError,errorMessage,"File exceeding 5mb");
         } 
    	public void validatePrposalError(String docSizeExceed,String changeRqst) throws Exception {
   		  validateFileUpload(docSizeExceed,changeRqst);
       	  scrollIntoView(txtComments, driver);
 	   	      takeScreenshot("Document file upload File Exceeding 5mb size"+"----"+changeRqst);
       	  takeScreenshot("Document file upload File Exceeding 5mb size");
       	  extentTest.log(LogStatus.INFO,"Add file button is selected and doc file having size exceeding 5mb is passed in mandatory field");
       	  assertText(fileProposalError,errorMessage,"File exceeding 5mb");
       } 

		 //*******************************************************************************// 
		   public PolicyBillingDetailsReviewPageAction validateDeductionFqcySet1(String changeRqst,String docurl,String pdfurl,String txturl,String xlsxurl,String csvurl,String docSizeExceed,String txtSizeExceed) throws Exception {
			   fileAdminOption.click();
			   validateAdminOptionsError(docSizeExceed, changeRqst);
			   fileAdminOption.click();
			   validateFileUpload(docurl, changeRqst);
			   fileUploadPolicy.click();
			   validatePolicyHolderErrorMsg(docSizeExceed, changeRqst);
			   fileUploadPolicy.click();
			   validateFileUpload(pdfurl, changeRqst);
			   fileUploadOption1.click();
			   validateOption1ErrorMsg(txtSizeExceed, changeRqst);
			   fileUploadOption1.click();
			   validateFileUpload(txturl, changeRqst);
			   fileUploadOption2.click();
			   validateFileUpload(csvurl, changeRqst);
			   fileUploadOption3.click();
			   validateFileUpload(xlsxurl, changeRqst);
			   validateContinue();
				return new PolicyBillingDetailsReviewPageAction();
				} 
		   public PolicyBillingDetailsReviewPageAction validateDeductionFqcySet2(String changeRqst,String pdfurl,String txturl,String docurl) throws Exception {
					
			   fileAdminOption.click();
			   validateFileUpload(pdfurl, changeRqst);
			   fileUploadPolicy.click();
			   validateFileUpload(txturl, changeRqst);
			   fileUploadOption1.click();
			   validateFileUpload(docurl, changeRqst);
			   fileUploadOption2.click();
			   validateFileUpload(pdfurl, changeRqst);
			   validateContinue();
			return new PolicyBillingDetailsReviewPageAction();
				}
			   public PolicyBillingDetailsReviewPageAction validateDeductionFqcySet3(String changeRqst,String pdfurl,String csvurl,String docurl) throws Exception {
					
				   fileAdminOption.click();
				   validateFileUpload(pdfurl, changeRqst);
				
				   fileUploadPolicy.click();
				   validateFileUpload(docurl, changeRqst);
				
					validateContinue();
					return new PolicyBillingDetailsReviewPageAction();
				}
			   
			
	//Method for selecting the coverage
	public void selectCoverage(String life,String add,String dlf,String dad,String vtl,
		     String vta,String dvt,String dva,String std,String ltd,String wds,String wdl,String ols) throws Exception {
		Thread.sleep(3000);
		
		if(life.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxLife,"Life");
		}
		if(add.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxADD,"ADD");
		}
		if(dlf.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxDLF,"DLF");
		}
		if(dad.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxDAD,"DAD");
		}
		
		if(vtl.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxVTL,"VTL");
		}
		if(vta.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxVTA,"VTA");
		}
		if(dvt.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxDVT,"DVT");
		}
		if(dva.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxDVA,"DVA");
		}
		takeScreenshot("Policy Billing Detail_CoverageSelectionUp");
		
		if(std.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxSTD,"STD");
		}
		if(ltd.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxLTD,"LTD");
		}
		if(wds.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxWDS,"WDS");
		}
		if(wdl.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxWDL,"WDL");
		}
		if(ols.equalsIgnoreCase("Yes")) {
			ClickElement(chckboxOLS,"OLS");
			
		}
	  scrollIntoView(chckboxActiveClaim, driver);
	  takeScreenshot("Policy Billing Detail_CoverageSelectionDown");
	}
	
    
    
   //method to upload file using Robot class 
    
  public void  validateFileUpload(String filepath,String changeRqst) throws Exception {
	  Robot robot=new Robot();
	  StringSelection stringSelection=new StringSelection(filepath);
	  Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection,null);
	  robot.setAutoDelay(1000);
	  robot.keyPress(KeyEvent.VK_CONTROL);
	  robot.keyPress(KeyEvent.VK_V);
	  robot.keyRelease(KeyEvent.VK_CONTROL);
	  robot.keyRelease(KeyEvent.VK_V);
	  robot.keyPress(KeyEvent.VK_ENTER);
	  robot.keyRelease(KeyEvent.VK_ENTER);
	  takeScreenshot("FileUpload Screen"+"---"+changeRqst);
	  extentTest.log(LogStatus.INFO,"Add file button is selected and File is Uploaded");
    }

  //method for selecting Continue button
   public void validateContinue() throws Exception {
	scrollPageDown(driver);
	ClickElement(btnContinue,"Continue");
	Thread.sleep(3000);
   }
	}
