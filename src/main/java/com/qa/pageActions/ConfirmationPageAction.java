package com.qa.pageActions;
import com.qa.page.ConfirmationPage;
import com.qa.page.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class ConfirmationPageAction extends ConfirmationPage {
	public ConfirmationPageAction() {
		super();
	}
	String message= "Your account change or service inquiry has been submitted. You will receive an email with ticket number for reference. A member of our team will reach out if additional information is required.";
	String ThankyouMsg= "Thank you for choosing OneAmerica.";
	String ErrorMessage="OneAmerica is committed to providing our customers with a hassle-free way to submit account updates and inquiries. Use this easy-to-navigate form to request changes such as name or address updates, salary updates, coverage elections, terminations and more.";
	/* *****************************************************************************
	  * Test Name : ConfirmationPageAction 
	  * Purpose : This class contains the action of  the Confirmation page
	  * History : Created by Anjali Johny on 09/27/2021 
	  **************************************************************************************/
	public PolicyBillingDetailPageAction validateConfirmation() throws InterruptedException {
		assertText(headerSuccess,"Success","Success header");
		takeScreenshot("Policy Billing Detail_Success message");
		String successMessage=txtMessage.getText();

		try {
		
		if(successMessage.equalsIgnoreCase(message)) {
			 extentTest.log(LogStatus.PASS,successMessage+" message is displayed");
			
		}
		else {
			extentTest.log(LogStatus.FAIL,successMessage+" message is not displayed, which is not expected");
		}
		}
		catch(Exception e) {
		
			extentTest.log(LogStatus.FAIL,successMessage+" message is not displayed, which is not expected");
		
		}
		
		String ThankyouMessage=txtPolicyChangesThankyouMsg.getText();
		try {
			
			if(ThankyouMessage.equalsIgnoreCase(ThankyouMsg)) {
				 extentTest.log(LogStatus.PASS,ThankyouMessage+" message is displayed");
				
			}
			else {
				extentTest.log(LogStatus.FAIL,ThankyouMessage+" message is not displayed, which is not expected");
			}
			}
			catch(Exception e) {
			
				extentTest.log(LogStatus.FAIL,ThankyouMessage+" message is not displayed, which is not expected");
			
			}
		
		takeScreenshot("Policy Billing Detail_Make New Request button");
	    assertElementDisplayed(btnMakeNewRequest,"Make a New Request button");
	    ClickElement(btnMakeNewRequest,"Make a New Request button");
	    
	    Thread.sleep(2000);
	return new PolicyBillingDetailPageAction();
	}
	public HomePageAction validateAccountChangesConfirmation() throws InterruptedException {
		assertText(headerSuccess,"Success","Success header");
		takeScreenshot("Account Changes_Success message");
		String successMessage=txtAccountChnagesMessage.getText();

		try {
		
		if(successMessage.equalsIgnoreCase(message)) {
			 extentTest.log(LogStatus.PASS,successMessage+" message is displayed");
			
		}
		else {
			assertText(headerError,"Error","Error Header");
			takeScreenshot("Policy Billing Detail_Error message");
			String ErrorMessage=txtAccountChnagesMessage.getText();
			extentTest.log(LogStatus.FAIL,ErrorMessage+" Unexpected Error");
		}
		}
		catch(Exception e) {
		
			extentTest.log(LogStatus.FAIL,successMessage+" message is not displayed, which is not expected");
		
		}
		
		String ThankyouMessage=txtAccountChangesThankyouMessage.getText();
		try {
			
			if(ThankyouMessage.equalsIgnoreCase(ThankyouMsg)) {
				 extentTest.log(LogStatus.PASS,ThankyouMessage+" message is displayed");
				
			}
			else {
				extentTest.log(LogStatus.FAIL,ThankyouMessage+" message is not displayed, which is not expected");
			}
			}
			catch(Exception e) {
			
				extentTest.log(LogStatus.FAIL,ThankyouMessage+" message is not displayed, which is not expected");
			
			}
	 
		takeScreenshot("Account Changes_Make New Request button");
	    assertElementDisplayed(btnMakeNewRequest,"Make a New Request button");
	    ClickElement(btnMakeNewRequest,"Make a New Request button");
	    
	    Thread.sleep(2000);
	return new HomePageAction();
	}
	}