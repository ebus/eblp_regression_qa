package com.qa.pageActions;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.qa.page.HomePage;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;


public class HomePageAction extends HomePage {
	public HomePageAction() {
		super();
	}
	
	/* *****************************************************************************
	  * Test Name : validateLeftNav 
	  * Purpose : This class contains the action of  the hOme page to perform left Navigation
	  * History : Created by Anjali Johny on 09/28/2021 
	  **************************************************************************************/
	public PolicyBillingDetailPageAction validateLeftNav(String userRole) throws Exception {
		Thread.sleep(2000);	
		assertElementDisplayed(linkHome,"Home Link");
		if (userRole.equalsIgnoreCase("Home office user")) {
		Thread.sleep(2000);
		ClickElement(linkOurPlans,"Our Plans link");
		Thread.sleep(1000);
		}
		else if (userRole.equalsIgnoreCase("PolicyHolder user")) {
			Thread.sleep(2000);
			ClickElement(linkMyEmp,"My Employees link");
			Thread.sleep(1000);
		}
		else if ((userRole.equalsIgnoreCase("Producer user"))||(userRole.equalsIgnoreCase("Producer Delegate user"))) {
			Thread.sleep(2000);
			ClickElement(linkMyBusiness,"My Business link");
			Thread.sleep(1000);
		}
		takeScreenshot("Home Page Left Navigation");
		ClickElement(sublinkPolicyBillingChanges,"Policy/Billing Changes link");
		return new PolicyBillingDetailPageAction();
	}
	
	
	public AccountChangesServiceInquiresPageAction validateAccountChangesLeftNav(String userRole) throws Exception {
		Thread.sleep(2000);	
		assertElementDisplayed(linkHome,"Home Link");
		if (userRole.equalsIgnoreCase("Home office user")) {
		Thread.sleep(2000);
		ClickElement(linkOurPlans,"Our Plans link");
		Thread.sleep(1000);
		}
		else if (userRole.equalsIgnoreCase("PolicyHolder user")) {
			Thread.sleep(2000);
			ClickElement(linkMyEmp,"My Employees link");
			Thread.sleep(1000);
		}
		else if ((userRole.equalsIgnoreCase("Producer user"))||(userRole.equalsIgnoreCase("Producer Delegate user"))) {
			Thread.sleep(2000);
			ClickElement(linkMyBusiness,"My Business link");
			Thread.sleep(1000);
		}
		takeScreenshot("Home Page Left Navigation");
		ClickElement(sublinkAccountServiceInquiresChanges,"Account Changes/Service Inquiries");
		return new AccountChangesServiceInquiresPageAction();
	}
	
//This method is to refresh and clicking on left navigation elements to enter values again
public void ValidateSideLink(String userRole) throws Exception {
	
if ((userRole.equalsIgnoreCase("Producer user")) || (userRole.equalsIgnoreCase("Home office user")) || 
(userRole.equalsIgnoreCase("PolicyHolder user")))
{
	driver.navigate().refresh();
	
}
else if (userRole.equalsIgnoreCase("Producer Delegate user")) {
	driver.navigate().refresh();
	Thread.sleep(1000);
	ClickElement(linkMyBusiness,"My Business link");
	Thread.sleep(1000);
	ClickElement(linkAccountChanges,"Account Changes/Service Inquiries");
	Thread.sleep(2000);
	ClickElement(sublinkPolicyBillingChanges,"Policy/Billing Changes link");
}
}                                   

//method to click on Account Changes/Service Inquiries link 
public void ValidateAccountChangeSideLink(String userRole) throws Exception {
	
if ((userRole.equalsIgnoreCase("Producer user")) || (userRole.equalsIgnoreCase("Home office user")) || 
(userRole.equalsIgnoreCase("PolicyHolder user")))
{
	driver.navigate().refresh();
	
}
else if (userRole.equalsIgnoreCase("Producer Delegate user")) {
	driver.navigate().refresh();
	Thread.sleep(1000);
	ClickElement(linkMyBusiness,"My Business link");
	Thread.sleep(1000);
	ClickElement(sublinkPolicyBillingChanges,"Policy/Billing Changes link");
	Thread.sleep(2000);
	ClickElement(sublinkAccountServiceInquiresChanges,"Account Changes/Service Inquiries link");
}
}
	
//method to perform log out
	public void userLogout() throws InterruptedException {
		ClickElement(linkLogoutMenu, "User Menu for Logout");
		takeScreenshot("userMenu");
		Thread.sleep(3000);
		ClickElement(linkLogout, "Logout link");
		Thread.sleep(3000);
		
	}


	}