package com.qa.pageActions;

import com.qa.page.PolicyBillingDetailsReviewPage;
import com.relevantcodes.extentreports.LogStatus;


public class PolicyBillingDetailsReviewPageAction extends PolicyBillingDetailsReviewPage {
	public PolicyBillingDetailsReviewPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : PolicyBillingDetailsReviewPageAction 
	  * Purpose : This class contains the action of  the PolicyBillingDetailsReview page
	  * History : Created by Anjali Johny on 09/27/2021 
	  **************************************************************************************/
public ConfirmationPageAction validateReviewPage() throws Exception {
	Thread.sleep(1000);
    scrollPageUp(driver);
    takeScreenshot("Policy Billing Details Review_Screen1");
    assertElementDisplayed(headerPolicyInfo,"Policy Information header");
    scrollIntoView(headerPolicyInfo, driver);
    takeScreenshot("Policy Billing Details Review_Screen2");
    assertElementDisplayed(headerRequesterInfo,"Requester Information header");
    scrollIntoView(headerRequesterInfo, driver);
    takeScreenshot("Policy Billing Details Review_Screen3");
    assertElementDisplayed(headerTypeOfRqst,"Type of Request header");
	assertElementDisplayed(btnEdit,"Edit button");
	assertElementDisplayed(btnSubmit,"Submit button");
	scrollIntoView(btnSubmit, driver);
	takeScreenshot("Policy Billing Details Review_Screen4");
	extentTest.log(LogStatus.INFO,"Policy Billing Detail page - Review page is displayed");
    ClickElement(btnSubmit,"Submit Button");
	return new ConfirmationPageAction();
}	
	}